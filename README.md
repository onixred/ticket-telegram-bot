# README #

The aim of the project is to provide an interface for interaction with the notification-ticket-porkha project via telegram

### Basic algorithm ###
* Listen to messages from the user
* Translate messages and send to the server (notification-ticket-purchase)
* Receive reply and send messages to the user

### Basic settings in the file: ###
* ticket-telegram-bot.properties this configuration for the application
* log4j2.xml this configurationfor logging

### Contribution guidelines ###
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* My email onixbed@gmail.com