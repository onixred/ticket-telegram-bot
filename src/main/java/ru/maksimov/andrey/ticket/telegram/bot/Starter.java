package ru.maksimov.andrey.ticket.telegram.bot;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.ComponentScan;

/**
 * Прототип "botА уведомления о билетах "
 * 
 * @author amaksimov
 */
@SpringBootApplication()
@ComponentScan({ "ru.maksimov.andrey.commons", "ru.maksimov.andrey.ticket", "ru.maksimov.andrey.notification.ticket.purchase.client" })
public class Starter {

	private static final Logger LOG = LogManager.getLogger(Starter.class);

	public static void main(String[] args) {
		SpringApplication.run(new Class<?>[] { Starter.class }, args);
		LOG.info("APPLICATION STARTED");
	}
}
