package ru.maksimov.andrey.ticket.telegram.bot.chart;

import java.util.ArrayList;
import java.util.List;

/**
 * Диаграммы в байтах
 * 
 * @author amaksimov
 */
public class Charts {

	private String price2Time;
	private String freePlaces2Time;

	public String getPrice2Time() {
		return price2Time;
	}

	public String getFreePlaces2Time() {
		return freePlaces2Time;
	}

	public void setPrice2Time(String price2Time) {
		this.price2Time = price2Time;
	}

	public void setFreePlaces2Time(String freePlaces2Time) {
		this.freePlaces2Time = freePlaces2Time;
	}

	/**
	 * Получить список диаграмм первая цена/время, вторая свободные места/время
	 */
	public List<String> getList() {
		List<String> list = new ArrayList<String>();
		if (price2Time != null) {
			list.add(price2Time);
		}
		if (freePlaces2Time != null) {
			list.add(freePlaces2Time);
		}
		return list;
	}

}
