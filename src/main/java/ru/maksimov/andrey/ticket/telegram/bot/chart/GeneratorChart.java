package ru.maksimov.andrey.ticket.telegram.bot.chart;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.codec.binary.Base64;
import org.knowm.xchart.BitmapEncoder;
import org.knowm.xchart.BitmapEncoder.BitmapFormat;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYSeries.XYSeriesRenderStyle;

import ru.maksimov.andrey.notification.ticket.purchase.client.dto.ArchiveFreePlaceDto;

/**
 * Генератор диаграмм
 * 
 * @author amaksimov
 */
public class GeneratorChart {

	static int WIDTH = 800;
	static int HEIGHT = 600;

	public static Charts getLineChart(Map<String,Map<String, List<ArchiveFreePlaceDto>>> placeType2places, String title)
			throws IOException {

		Map<String, List<ArchiveFreePlaceDto>> sortedPlaceTypeAndCarName2places = new HashMap<String, List<ArchiveFreePlaceDto>>();
		for (String placeType : placeType2places.keySet()) {
			Map<String, List<ArchiveFreePlaceDto>> carName2Places = placeType2places.get(placeType);
			for (String carName : carName2Places.keySet()) {
				List<ArchiveFreePlaceDto> places = carName2Places.get(carName);
				List<ArchiveFreePlaceDto> sortedPlaces = places.stream()
						.sorted((object1, object2) -> object1.getDateUpdate().compareTo(object2.getDateUpdate()))
						.collect(Collectors.toList());
				sortedPlaceTypeAndCarName2places.put(placeType +  " " + carName, sortedPlaces);
			}
		}

		String formatDate = "HH:mm dd.MM";

		XYChart chartFreePlaces2Time = new XYChart(WIDTH, HEIGHT);
		chartFreePlaces2Time.setTitle(title);
		chartFreePlaces2Time.setYAxisTitle("места");
		chartFreePlaces2Time.setXAxisTitle("время");
		chartFreePlaces2Time.getStyler().setDefaultSeriesRenderStyle(XYSeriesRenderStyle.Line);
		chartFreePlaces2Time.getStyler().setDatePattern(formatDate);

		XYChart chartPrice2Time = new XYChart(WIDTH, HEIGHT);
		chartPrice2Time.setTitle(title);
		chartPrice2Time.setYAxisTitle("рубли");
		chartPrice2Time.setXAxisTitle("время");
		chartPrice2Time.getStyler().setDefaultSeriesRenderStyle(XYSeriesRenderStyle.Line);
		chartPrice2Time.getStyler().setDatePattern(formatDate);

		for (String placeTypeAndCarName : sortedPlaceTypeAndCarName2places.keySet()) {
			List<ArchiveFreePlaceDto> places = sortedPlaceTypeAndCarName2places.get(placeTypeAndCarName);

			List<Double> prices = new ArrayList<Double>();
			List<Integer> freePlaces = new ArrayList<Integer>();
			List<Date> xAges = new ArrayList<Date>();
			for (ArchiveFreePlaceDto place : places) {
				prices.add(place.getTariff());
				freePlaces.add(place.getFreeNumber());
				xAges.add(place.getDateUpdate());
			}

			chartPrice2Time.addSeries(placeTypeAndCarName, xAges, prices);
			chartFreePlaces2Time.addSeries(placeTypeAndCarName, xAges, freePlaces);
		}

		byte[] price2TimeBytes = BitmapEncoder.getBitmapBytes(chartPrice2Time, BitmapFormat.PNG);
		String price2Time = Base64.encodeBase64String(price2TimeBytes);

		byte[] freePlaces2TimeBytes = BitmapEncoder.getBitmapBytes(chartFreePlaces2Time, BitmapFormat.PNG);
		String freePlaces2Time = Base64.encodeBase64String(freePlaces2TimeBytes);

		Charts charts = new Charts();
		charts.setFreePlaces2Time(freePlaces2Time);
		charts.setPrice2Time(price2Time);
		return charts;
	}}
