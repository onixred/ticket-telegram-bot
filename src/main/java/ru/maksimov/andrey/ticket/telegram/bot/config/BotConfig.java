package ru.maksimov.andrey.ticket.telegram.bot.config;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Configuration;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.bots.TelegramWebhookBot;
import org.telegram.telegrambots.exceptions.TelegramApiRequestException;
import org.telegram.telegrambots.generics.BotSession;

import ru.maksimov.andrey.commons.exception.BusinessException;
import ru.maksimov.andrey.ticket.telegram.bot.handler.HandlerLongPolling;
import ru.maksimov.andrey.ticket.telegram.bot.handler.HandlerWebhook;

/**
 * Конфигурация бота
 * 
 * @author amaksimov
 */
@Configuration
@ConditionalOnBean({ TelegramWebhookBot.class})
public class BotConfig {

	static {
		ApiContextInitializer.init();
	}

	private List<BotSession> sessions = new ArrayList<>();

	private Config config = Config.getConfig();

	@Autowired
	HandlerWebhook webhookService;

	@Autowired
	HandlerLongPolling handler;

	private TelegramBotsApi api;

	@PostConstruct
	public void start() throws BusinessException {
		if(config.isWebhook()) {
			//settings Webhook
			int port = config.getWebhookPort();
			String externalWebhookUrl = config.getWebhookUrlHost() + ":" + port;
			String internalWebhookUrl = config.getWebhookInternalUrlHost() + ":" + port;
			String pathToCertificatePublicKey = config.getWebhookPathToCertificate();
			String pathToCertificateStore = config.getWebhookPathToCertificateStore();
			String pathToCertificateStorePassword = config.getWebhookPathToCertificateStorePasswordPassword();
			TelegramBotsApi api = null;
			try {
				api = new TelegramBotsApi(pathToCertificateStore, pathToCertificateStorePassword, externalWebhookUrl,
						internalWebhookUrl, pathToCertificatePublicKey);
			} catch (TelegramApiRequestException tare) {
				throw new BusinessException("unable create Telegram bots api while Webhook ", tare);
			}
			try {
				api.registerBot(webhookService);
			} catch (TelegramApiRequestException tare) {
				throw new BusinessException("unable register bot ", tare);
			}
		} else {
			//settings long polling
			api = new TelegramBotsApi();
			try {
				api.registerBot(handler);
			} catch (TelegramApiRequestException tare) {
				throw new BusinessException("unable register bot ", tare);
			}
		}
	}

	@PreDestroy
	public void stop() {
		sessions.stream().forEach(session -> {
			if (session != null) {
				session.stop();
			}
		});
	}
}
