package ru.maksimov.andrey.ticket.telegram.bot.config;

import java.io.File;
import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ru.maksimov.andrey.commons.exception.VerificationException;

/**
 * Конфиг для приложения динамический
 * 
 * @author amaksimov
 */
public class Config {

	private static final Logger LOG = LogManager.getLogger(Config.class);

	private static final String FILE_NAME = "/ticket-telegram-bot.properties";

	private static final String TOKEN = "bot.token";
	private static final String USERNAME = "bot.username";
	private static final String IS_WEBHOOK = "bot.setting.requests";

	private static final String WEBHOOK_URL_HOST = "webhook.url.host";
	private static final String WEBHOOK_INTERNAL_URL_HOST = "webhook.internal.url.host";
	private static final String WEBHOOK_PORT = "webhook.port";
	private static final String WEBHOOK_PATH_CERTIFICATE = "webhook.path.certificate";
	private static final String WEBHOOK_PATH_CERTIFICATE_STORE = "webhook.path.certificate.store";
	private static final String WEBHOOK_PATH_CERTIFICATE_STORE_PASSWORD = "webhook.certificate.store.password";

	private static final String NOTIFICATION_TICKET_PURCHASE_URL_HOST = "notification.ticket.purchase.url.host";

	private static final String MAX_LIST_MENU = "max.list.menu";
	private static final String MAX_SENDING_MESASAGE = "max.sending.mesasage";
	private static final String TASKS_LIMIT_FOR_USER = "tasks.limit.for.user";
	private static final String TASKS_LIMIT_IGNORE_USERS = "tasks.limit.ignore.users";

	private static final String TASKS_LIMIT = "tasks.limit";
	private static final String TIMETABLE_LIMIT = "timetable.limit";

	private static final Config INSTANCE = new Config();
	private Configuration configuration;

	private String token;
	private String username;
	private boolean isWebhook;

	private String webhookUrlHost;
	private String webhookInternalUrlHost;
	private Integer webhookPort;
	private String webhookPathToCertificate;
	private String webhookPathToCertificateStore;
	private String webhookPathToCertificateStorePassword;

	private String notificationTicketPurchaseUrlHost;

	private int maxListMenu;
	private int maxSendingMesasage;
	private int tasksLimitForUser;
	private Set<String> tasksLimitIgnoreUsers = new HashSet<String>();
	private int tasksLimit;
	private int timetableLimit;

	private Config() {
		loadConfig();
	}

	/**
	 * @return экземпляр конфига
	 */
	public static Config getConfig() {
		return INSTANCE;
	}

	/**
	 * @return перезагрузить конфиг
	 */
	public void reload() {
		loadConfig();
	}

	/**
	 * Загрузка конфига
	 */
	private void loadConfig() {
		String propertiesFile = null;
		String property = "ticket-telegram-bot.properties";
		try {
			propertiesFile = System.getProperty(property);
		} catch (Exception e) {
			LOG.warn("FATAL: can't load System property: " + property);
		}

		try {
			URL resource;
			if (StringUtils.isBlank(propertiesFile)) {
				resource = Config.class.getResource(FILE_NAME);
			} else {
				resource = new File(propertiesFile).toURI().toURL();
			}
			configuration = new PropertiesConfiguration(resource);
			init();
		} catch (Exception e) {
			throw new RuntimeException("FATAL: can't load config from: " + FILE_NAME, e);
		}
	}

	/**
	 * Иницилизация параметров конфига
	 * 
	 * @throws VerificationException
	 */
	private void init() throws VerificationException {
		setToken(configuration.getString(TOKEN));
		setUsername(configuration.getString(USERNAME));
		setWebhook(configuration.getBoolean(IS_WEBHOOK));
		setWebhookUrlHost(configuration.getString(WEBHOOK_URL_HOST));
		setWebhookInternalUrlHost(configuration.getString(WEBHOOK_INTERNAL_URL_HOST));
		setWebhookPort(configuration.getInt(WEBHOOK_PORT));
		setWebhookPathToCertificate(configuration.getString(WEBHOOK_PATH_CERTIFICATE));
		setWebhookPathToCertificateStore(configuration.getString(WEBHOOK_PATH_CERTIFICATE_STORE));
		setWebhookPathToCertificateStorePassword(configuration.getString(WEBHOOK_PATH_CERTIFICATE_STORE_PASSWORD));
		setNotificationTicketPurchaseUrlHost(configuration.getString(NOTIFICATION_TICKET_PURCHASE_URL_HOST));
		setMaxListMenu(configuration.getInt(MAX_LIST_MENU));
		setTasksLimitForUser(configuration.getInt(TASKS_LIMIT_FOR_USER));
		List<Object> ignoreUsers = configuration.getList(TASKS_LIMIT_IGNORE_USERS);
		for(Object ignoreUser: ignoreUsers) {
			getTasksLimitIgnoreUsers().add(ignoreUser.toString());
		}
		setTasksLimit(configuration.getInt(TASKS_LIMIT));
		setTimetableLimit(configuration.getInt(TIMETABLE_LIMIT));
		setMaxSendingMesasage(configuration.getInt(MAX_SENDING_MESASAGE));
	}

	private void setToken(String token) {
		this.token = token;
	}

	/**
	 * Получить токен
	 * 
	 * @return токен
	 */
	public String getToken() {
		return token;
	}

	private void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Получить призак веб-хук
	 * 
	 * @return призак использоваать веб-хук
	 */
	public boolean isWebhook() {
		return isWebhook;
	}

	private void setWebhook(boolean isWebhook) {
		this.isWebhook = isWebhook;
	}

	/**
	 * Получить имя бота
	 * 
	 * @return имя
	 */
	public String getUsername() {
		return username;
	}

	private void setWebhookPort(Integer webhookPort) {
		this.webhookPort = webhookPort;
	}

	/**
	 * Получить порт для веб хука
	 * 
	 * @return порт
	 */
	public Integer getWebhookPort() {
		return webhookPort;
	}

	private void setWebhookUrlHost(String webhookUrlHost) {
		this.webhookUrlHost = webhookUrlHost;
	}

	/**
	 * Получить url-адрес бота
	 * 
	 * @return url-адрес
	 */
	public String getWebhookUrlHost() {
		return webhookUrlHost;
	}

	private void setWebhookInternalUrlHost(String webhookInternalUrlHost) {
		this.webhookInternalUrlHost = webhookInternalUrlHost;
	}

	/**
	 * Получить внутрений url-адрес бота
	 * 
	 * @return url-адрес
	 */
	public String getWebhookInternalUrlHost() {
		return webhookInternalUrlHost;
	}

	private void setWebhookPathToCertificate(String webhookPathToCertificate) {
		this.webhookPathToCertificate = webhookPathToCertificate;
	}

	/**
	 * Получить путь до сертификата
	 * 
	 * @return путь до сертификата
	 */
	public String getWebhookPathToCertificate() {
		return webhookPathToCertificate;
	}

	private void setWebhookPathToCertificateStore(String webhookPathToCertificateStore) {
		this.webhookPathToCertificateStore = webhookPathToCertificateStore;
	}

	/**
	 * Получить путь до ханилища сертификата
	 * 
	 * @return путь до ханилища сертификата
	 */
	public String getWebhookPathToCertificateStore() {
		return webhookPathToCertificateStore;
	}

	private void setWebhookPathToCertificateStorePassword(String webhookPathToCertificateStorePassword) {
		this.webhookPathToCertificateStorePassword = webhookPathToCertificateStorePassword;
	}

	/**
	 * Получить пароль от хранилища
	 * 
	 * @return пароль
	 */
	public String getWebhookPathToCertificateStorePasswordPassword() {
		return webhookPathToCertificateStorePassword;
	}

	private void setNotificationTicketPurchaseUrlHost(String notificationTicketPurchaseUrlHost) {
		this.notificationTicketPurchaseUrlHost = notificationTicketPurchaseUrlHost;
	}

	/**
	 * Получить url-адрес NotificationTicketPurchase
	 * 
	 * @return url-адрес
	 */
	public String getNotificationTicketPurchaseUrlHost() {
		return notificationTicketPurchaseUrlHost;
	}

	private void setMaxListMenu(int maxListMenu) {
		this.maxListMenu = maxListMenu;
	}

	/**
	 * Получить максимальное число пунктов в списке меню
	 * 
	 * @return дупустимый максимальный список в меню
	 */
	public int getMaxListMenu() {
		return maxListMenu;
	}

	private void setTasksLimitForUser(int tasksLimitForUser) {
		this.tasksLimitForUser = tasksLimitForUser;
	}

	/**
	 * Получить предел по количеству задач для одногно пользователя
	 * 
	 * @return предел по задачам
	 */
	public int getTasksLimitForUser() {
		return tasksLimitForUser;
	}

	public Set<String> getTasksLimitIgnoreUsers() {
		return tasksLimitIgnoreUsers;
	}

	private void setTasksLimit(int tasksLimit) {
		this.tasksLimit = tasksLimit;
	}

	/**
	 * Получить предел по количеству задач (ограничение на время теста)
	 * 
	 * @return предел по задачам
	 */
	public int getTasksLimit() {
		return tasksLimit;
	}

	private void setTimetableLimit(int timetableLimit) {
		this.timetableLimit = timetableLimit;
	}

	/**
	 * Получить предел по количеству временных интерваалов оповещения для
	 * одногно пользователя
	 * 
	 * @return предел по задачам
	 */
	public int getTimetableLimit() {
		return timetableLimit;
	}

	private void setMaxSendingMesasage(int maxSendingMesasage) {
		this.maxSendingMesasage = maxSendingMesasage;
	}

	/**
	 * Получить максимальное количество сообщений которое можно отправиить за
	 * один раз
	 * 
	 * @return максимальное количество
	 */
	public int getMaxSendingMesasage() {
		return maxSendingMesasage;
	}

}
