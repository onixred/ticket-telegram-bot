package ru.maksimov.andrey.ticket.telegram.bot.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import ru.maksimov.andrey.notification.ticket.purchase.client.NotificationTicketPurchaseClient;

/**
 * Конфигурация spring"
 * 
 * @author amaksimov
 */
@Configuration
@EnableJpaRepositories(basePackages = "ru.maksimov.andrey.ticket.telegram.bot.repository")
@EnableAutoConfiguration
@EntityScan(basePackages = { "ru.maksimov.andrey.ticket.telegram.bot.entity" })

public class SpringConfig {

	private Config config = Config.getConfig();

	@Bean
	public NotificationTicketPurchaseClient notificationTicketPurchaseClient() {
		return new NotificationTicketPurchaseClient(config.getNotificationTicketPurchaseUrlHost());
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate(clientHttpRequestFactory());
	}

	private ClientHttpRequestFactory clientHttpRequestFactory() {
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
		factory.setReadTimeout(120000);
		factory.setConnectTimeout(2000);
		return factory;
	}
}
