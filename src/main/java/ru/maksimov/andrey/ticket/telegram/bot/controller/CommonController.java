package ru.maksimov.andrey.ticket.telegram.bot.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ru.maksimov.andrey.commons.exception.VerificationException;
import ru.maksimov.andrey.commons.log.Loggable;
import ru.maksimov.andrey.notification.ticket.purchase.client.Response;
import ru.maksimov.andrey.notification.ticket.purchase.client.TicketTelegramBotClient;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.ArchiveTicketsDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.MessageDto;

import ru.maksimov.andrey.ticket.telegram.bot.service.MessageService;

/* Основной контроллер 
 * 
 * @author amaksimov
 */
@RestController
@RequestMapping("rest")
public class CommonController {

	@Autowired
	MessageService messageService;

	@Loggable
	@RequestMapping(value = TicketTelegramBotClient.NOTIFICATION_SEND, method = RequestMethod.POST)
	public Response<Void> notificationSend(@RequestBody MessageDto messageDto) throws VerificationException {
		Response<Void> response = new Response<Void>();
		try {
			messageService.notificationSend(messageDto);
		} catch (VerificationException ve) {
			response = new Response<Void>(503, "Unable notification send. " + ve.getMessage(), null);
		}
		return response;
	}

	@Loggable
	@RequestMapping(value = TicketTelegramBotClient.ARCHIVE_TICKETS_SEND, method = RequestMethod.POST)
	public Response<Void> archiveTicketsSend(@RequestBody ArchiveTicketsDto archiveTicketsDto) throws VerificationException {
		Response<Void> response = new Response<Void>();
		try {
			messageService.archiveTicketsSend(archiveTicketsDto);
		} catch (VerificationException ve) {
			response = new Response<Void>(503, "Unable archive tickets send. " + ve.getMessage(), null);
		}
		return response;
	}
}
