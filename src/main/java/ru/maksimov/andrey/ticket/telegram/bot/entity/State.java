package ru.maksimov.andrey.ticket.telegram.bot.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

/**
 * Сущность состояние
 * 
 * @author amaksimov
 */
@Entity
@Table(name = "state")
public class State {

	@Id
	@GeneratedValue(generator = "stateSequenceGenerator")
	@GenericGenerator(name = "stateSequenceGenerator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@Parameter(name = "sequence_name", value = "STATE_SEQUENCE"),
			@Parameter(name = "initial_value", value = "1000"), @Parameter(name = "increment_size", value = "1") })
	private Long id;
	@Column(nullable = false)
	private int userId;
	@Column(nullable = false)
	private long chatId;
	private String userName;
	@Enumerated(EnumType.STRING)
	private StateType type;
	private boolean isRegistered;
	private boolean isDeleted;

	private String mapCommand2id;

	@OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private TaskBot taskBot;

	/**
	 * Получить идентификатор статуса
	 * 
	 * @return идентификатор
	 */
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Получить идентификатор пользователя в telegram
	 * 
	 * @return идентификатор пользователя
	 */
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * Получить идентификатор чата в telegram
	 * 
	 * @return идентификатор чата
	 */
	public long getChatId() {
		return chatId;
	}

	public void setChatId(long chatId) {
		this.chatId = chatId;
	}

	/**
	 * Получитьимя пользователя в telegram
	 * 
	 * @return идентификатор чата
	 */
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Получить тип состояния
	 * 
	 * @return тип
	 */
	public StateType getType() {
		return type;
	}

	public void setType(StateType type) {
		this.type = type;
	}

	/**
	 * Строка сериализованной кары key - comand, value - id в БД. Может быть null.
	 * 
	 * @return строка сериализованной карты
	 */
	public String getMapCommand2id() {
		return mapCommand2id;
	}

	public void setMapCommand2id(String mapCommand2id) {
		this.mapCommand2id = mapCommand2id;
	}

	/**
	 * Задача для бота
	 * 
	 * @return Задача для бота
	 */
	public TaskBot getTaskBot() {
		return taskBot;
	}

	public void setTaskBot(TaskBot taskBot) {
		this.taskBot = taskBot;
	}

	/**
	 * Признак регистрации пользователя в бек энде
	 * 
	 * @return true если зарезистрирован иначе false
	 */
	public boolean isRegistered() {
		return isRegistered;
	}

	public void setRegistered(boolean isRegistered) {
		this.isRegistered = isRegistered;
	}

	/**
	 * Признак удаленного бота
	 * 
	 * @return true если бот удален
	 */
	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
