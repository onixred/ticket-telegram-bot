package ru.maksimov.andrey.ticket.telegram.bot.entity;

/**
 * Перечисление тип статуса
 * 
 * @author <a href="mailto:onixbed@gmail.com">amaksimov</a>
 */
public enum StateType {
	START_STATE,
	MAIN_MENU,
	LIST_TASK,
	SELECT_TASK,
	INFO_TASK,
	CANCEL_TASK,
	ADD_TASK_STATION_1,
	LIST_STATION_1,
	ADD_TASK_STATION_2,
	LIST_STATION_2,
	ADD_TASK_DATE_START,
	ADD_TASK_CAR_TYPE,
	ADD_TASK_PLACE_TYPE,
	ADD_TASK_COUNT_PLACE,
	ADD_TASK_END,
	SETTINGS,
	LIST_TIMETABLE,
	SELECT_TIMETABLE,
	ADD_TIMETABLE
}