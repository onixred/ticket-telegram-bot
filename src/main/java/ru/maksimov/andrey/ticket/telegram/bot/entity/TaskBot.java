package ru.maksimov.andrey.ticket.telegram.bot.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import ru.maksimov.andrey.notification.ticket.purchase.client.type.CarType;
import ru.maksimov.andrey.notification.ticket.purchase.client.type.PlaceType;

/**
 * Сущность задача для бота
 * 
 * @author amaksimov
 */
@Entity
@Table(name = "task_bot")
public class TaskBot {

	@Id
	@GeneratedValue(generator = "taskBotSequenceGenerator")
	@GenericGenerator(name = "taskBotSequenceGenerator", strategy = "org.hibernate.id.enhanced.SequenceStyleGenerator", parameters = {
			@Parameter(name = "sequence_name", value = "TASK_BOT_SEQUENCE"),
			@Parameter(name = "initial_value", value = "1000"), @Parameter(name = "increment_size", value = "1") })
	private Long id;

	private Long stationStartId;
	private Long stationEndId;
	private Date startDate;
	@Enumerated(EnumType.STRING)
	private CarType carType;
	@Enumerated(EnumType.STRING)
	private PlaceType placeType;
	private Integer countPlace;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getStationStartId() {
		return stationStartId;
	}

	public void setStationStartId(Long stationStartId) {
		this.stationStartId = stationStartId;
	}

	public Long getStationEndId() {
		return stationEndId;
	}

	public void setStationEndId(Long stationEndId) {
		this.stationEndId = stationEndId;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public PlaceType getPlaceType() {
		return placeType;
	}

	public void setPlaceType(PlaceType placeType) {
		this.placeType = placeType;
	}

	public Integer getCountPlace() {
		return countPlace;
	}

	public void setCountPlace(Integer countPlace) {
		this.countPlace = countPlace;
	}

	public CarType getCarType() {
		return carType;
	}

	public void setCarType(CarType carType) {
		this.carType = carType;
	}
}
