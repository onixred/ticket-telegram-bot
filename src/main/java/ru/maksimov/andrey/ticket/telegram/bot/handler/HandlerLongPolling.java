package ru.maksimov.andrey.ticket.telegram.bot.handler;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.generics.LongPollingBot;

/**
 * Интерфейс обработчика "длинных запросов" 
 * 
 * @author amaksimov
 */
public interface HandlerLongPolling extends LongPollingBot {

	public Message sendMessage(SendMessage sendMessage) throws TelegramApiException;

}
