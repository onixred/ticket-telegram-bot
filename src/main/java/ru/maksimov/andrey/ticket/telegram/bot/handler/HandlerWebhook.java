package ru.maksimov.andrey.ticket.telegram.bot.handler;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import org.telegram.telegrambots.generics.WebhookBot;

/**
 * Интерфейс Веб-хук обработчика
 * 
 * @author amaksimov
 */
public interface HandlerWebhook extends WebhookBot {

	public Message sendMessage(SendMessage sendMessage) throws TelegramApiException;

}
