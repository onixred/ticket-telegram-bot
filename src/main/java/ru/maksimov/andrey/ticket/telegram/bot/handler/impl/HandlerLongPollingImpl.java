package ru.maksimov.andrey.ticket.telegram.bot.handler.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import ru.maksimov.andrey.ticket.telegram.bot.config.Config;
import ru.maksimov.andrey.ticket.telegram.bot.handler.HandlerLongPolling;
import ru.maksimov.andrey.ticket.telegram.bot.service.MessageService;

/**
 * Реализация обработчика "длинных запросов"
 * 
 * @author amaksimov
 */
@Service
public class HandlerLongPollingImpl extends TelegramLongPollingBot implements HandlerLongPolling {

	private static final Logger LOG = LogManager.getLogger(HandlerLongPollingImpl.class);

	private Config config = Config.getConfig();

	@Autowired
	private MessageService messageService;

	public HandlerLongPollingImpl() {
		super();
	}

	@Override
	public String getBotToken() {
		return config.getToken();
	}

	@Override
	public void onUpdateReceived(Update update) {
		try {
			if (update.hasMessage()) {
				Message message = update.getMessage();
				if (message.hasText() || message.hasLocation()) {
					messageService.handleIncomingMessage(message);
				}
			}
		} catch (Exception e) {
			LOG.error("Unable update received", e);
		}
	}

	@Override
	public String getBotUsername() {
		return config.getUsername();
	}

}