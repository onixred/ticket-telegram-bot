package ru.maksimov.andrey.ticket.telegram.bot.handler.impl;

import java.io.Serializable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.api.methods.BotApiMethod;

import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramWebhookBot;


import ru.maksimov.andrey.ticket.telegram.bot.config.Config;
import ru.maksimov.andrey.ticket.telegram.bot.handler.HandlerWebhook;
import ru.maksimov.andrey.ticket.telegram.bot.service.MessageService;

/**
 * Реализация веб-хук обработчика
 * 
 * @author amaksimov
 */
@Service
public class WebhookHandlerImpl extends TelegramWebhookBot implements HandlerWebhook {

	private static final Logger LOG = LogManager.getLogger(WebhookHandlerImpl.class);

	private Config config = Config.getConfig();

	@Autowired
	private MessageService messageService;

	@Override
	public BotApiMethod<? extends Serializable> onWebhookUpdateReceived(Update update) {
		try {
			if (update.hasMessage()) {
				Message message = update.getMessage();
				if (message.hasText() || message.hasLocation()) {
					messageService.handleIncomingMessage(message);
				}
			}
		} catch (Exception e) {
			LOG.error("Unable update received", e);
		}
		return null;
	}

	@Override
	public String getBotUsername() {
		return config.getUsername();
	}

	@Override
	public String getBotPath() {
		return config.getUsername();
	}

	@Override
	public String getBotToken() {
		return config.getToken();
	}

}
