package ru.maksimov.andrey.ticket.telegram.bot.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ru.maksimov.andrey.ticket.telegram.bot.entity.State;

/**
 * Интерфейс по работе c хранилищем состояний
 * 
 * @author amaksimov
 */
@Repository
public interface StateRepository extends CrudRepository<State, Long> {

	@Query("select s from State s where s.userId = ?1 and s.chatId = ?2")
	List<State> findAllByUserIdAndChatId(Integer userId, Long chatId);
}
