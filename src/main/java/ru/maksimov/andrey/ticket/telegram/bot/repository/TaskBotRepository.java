package ru.maksimov.andrey.ticket.telegram.bot.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ru.maksimov.andrey.ticket.telegram.bot.entity.TaskBot;

/**
 * Интерфейс по работе c хранилищем задач для бота
 * 
 * @author amaksimov
 */
@Repository
public interface TaskBotRepository extends CrudRepository<TaskBot, Long>{

}
