package ru.maksimov.andrey.ticket.telegram.bot.service;

import ru.maksimov.andrey.ticket.telegram.bot.entity.State;
import ru.maksimov.andrey.ticket.telegram.bot.entity.TaskBot;

/**
 * Интерфейс сервиса по работе c ботом
 * 
 * @author amaksimov
 */
public interface DatabaseService {

	/**
	 * Найти состояние по userId и chatId
	 * 
	 * @param userId
	 *             идентификатор пользователя в telegram
	 * @param chatId
	 *            идентификатор чата в telegram
	 * @return состояние
	 */
	public State findByUserIdAndChatIdAndUserName(Integer userId, Long chatId, String userName);

	/**
	 * Найти состояние по stateId
	 * 
	 * @param stateId
	 *             идентификатор состояния
	 * @return состояние
	 */
	public State findById(Long stateId);

	/**
	 * Сохранить\обновить состояние
	 * 
	 * @param state
	 *             состояние
	 * @return состояние
	 */
	public State save(State state);

	/**
	 * Сохранить\обновить задачу для бота
	 * 
	 * @param taskBot
	 *             задача для бота
	 * @return задача
	 */
	public TaskBot save(TaskBot taskBot);
}
