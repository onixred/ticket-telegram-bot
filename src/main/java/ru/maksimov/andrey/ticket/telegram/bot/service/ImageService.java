package ru.maksimov.andrey.ticket.telegram.bot.service;

import java.util.List;
import java.util.Map;

import ru.maksimov.andrey.commons.exception.BusinessException;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.ArchiveFreePlaceDto;

/**
 * Интерфейс сервиса по диаграммами
 * 
 * @author amaksimov
 */
public interface ImageService {

	final static int SLIEEP = 26000;

	List<String> getImageUrls(Map<String, Map<String, List<ArchiveFreePlaceDto>>> placeType2places, String title)
			throws BusinessException;

}
