package ru.maksimov.andrey.ticket.telegram.bot.service;

import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import ru.maksimov.andrey.commons.exception.VerificationException;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.ArchiveTicketsDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.MessageDto;

/**
 * Интерфейс сервиса по работе c сообщением
 * 
 * @author amaksimov
 */
public interface MessageService {

	void handleIncomingMessage(Message message) throws TelegramApiException;

	/**
	 * Отправить уведомление 
	 * 
	 * @param messageDto
	 *             сообщение
	 * @return сообщение
	 */
	void notificationSend(MessageDto messageDto) throws VerificationException;

	
	/**
	 * Отправить статистику из архива билетов 
	 * 
	 * @param messageDto
	 *             сообщение
	 * @return сообщение
	 */
	void archiveTicketsSend(ArchiveTicketsDto archiveTicketsDto) throws VerificationException;
	
}
