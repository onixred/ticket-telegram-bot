package ru.maksimov.andrey.ticket.telegram.bot.service;


/**
 * Интерфейс сервиса по работе с сущностью "задача"
 * 
 * @author amaksimov
 */
public interface TaskService {

	/**
	 * Добавить задачу в обработку.
	 * 
	 * @param task
	 *            задача
	 * @return список городов
	 */
	void addTasks(Runnable task);
}
