package ru.maksimov.andrey.ticket.telegram.bot.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import ru.maksimov.andrey.commons.exception.BusinessException;

import ru.maksimov.andrey.notification.ticket.purchase.client.dto.ArchiveFreePlaceDto;
import ru.maksimov.andrey.ticket.telegram.bot.chart.Charts;
import ru.maksimov.andrey.ticket.telegram.bot.chart.GeneratorChart;

import ru.maksimov.andrey.ticket.telegram.bot.service.ImageService;

import com.cloudinary.*;

/**
 * Сервис по работе с диаграммами через api cloudinary.com
 * 
 * @author amaksimov
 */
@Service(value = "CloudinaryImageService")
public class CloudinaryImageService implements ImageService {

	private static final Logger LOG = LogManager.getLogger(CloudinaryImageService.class);
	private static final String CLOUD_NAME = "onixred";
	private static final String API_KEY = "998851641332148";
	private static final String API_SECRET = "SwM2fZIaNKDTt_E7jcckU1bzLN0";
	//[<MIME-type>][;charset=<encoding>][;base64],<data>
	private static final String HEAD_IMAGE = "data:image/png;base64,";
	private static final String RESOURCE_TYPE_OPTION = "resource_type";
	private static final String TAGS_OPTION = "tags";

	private static final String TYPE_AUTO = "auto";
	private static final String TAG_RZD = "rzd";


	@Override
	public List<String> getImageUrls(Map<String, Map<String, List<ArchiveFreePlaceDto>>> placeType2places, String title)
			throws BusinessException {
		LOG.info("Start get image urls");
		List<String> urls = new ArrayList<String>();
		try {
			Charts charts = GeneratorChart.getLineChart(placeType2places, title);
			for (String image : charts.getList()) {
				int tryNumber = 0;
				int countLoad = 0;
				while (tryNumber < 5 && countLoad < 1) {
					Map<String, String> config = new HashMap<String, String>();
					config.put("cloud_name", CLOUD_NAME);
					config.put("api_key", API_KEY);
					config.put("api_secret", API_SECRET);
					Cloudinary cloudinary = new Cloudinary(config);
					try {
						Map<String, Object> options = new HashMap<String, Object>();
						options.put(RESOURCE_TYPE_OPTION, TYPE_AUTO);
						options.put(TAGS_OPTION, title +", " + TAG_RZD);

						@SuppressWarnings("unchecked")
						Map<String, Object> uploadResult = cloudinary.uploader().upload(HEAD_IMAGE + image, options);
						if (uploadResult == null) {
							LOG.warn("Unable upload chart: response is null");
						} else if (StringUtils.isNotBlank((String) uploadResult.get("url"))) {
							String url = (String) uploadResult.get("url");
							urls.add(url);
							countLoad++;
						} else {
							LOG.warn("Unable upload chart: " + Arrays.toString(uploadResult.keySet().toArray()));
						}
					} catch (IOException se) {
						LOG.warn("Unable upload chart: error execute post " + se.getMessage(), se);
					}

					try {
						Thread.sleep(SLIEEP * tryNumber + 100);
					} catch (InterruptedException ie) {
						throw new BusinessException("Unable upload chart: thread sleep: " + ie.getMessage(), ie);
					}
					tryNumber++;
				}
			}
		} catch (IOException ioe) {
			throw new BusinessException("Unable upload chart: error generator chart " + ioe.getMessage(), ioe);
		}
		LOG.info("End get image urls");
		return urls;
	}

}
