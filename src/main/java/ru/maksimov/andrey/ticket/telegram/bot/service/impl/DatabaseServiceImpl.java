package ru.maksimov.andrey.ticket.telegram.bot.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.maksimov.andrey.commons.log.Loggable;
import ru.maksimov.andrey.ticket.telegram.bot.entity.State;
import ru.maksimov.andrey.ticket.telegram.bot.entity.StateType;
import ru.maksimov.andrey.ticket.telegram.bot.entity.TaskBot;
import ru.maksimov.andrey.ticket.telegram.bot.repository.StateRepository;
import ru.maksimov.andrey.ticket.telegram.bot.repository.TaskBotRepository;
import ru.maksimov.andrey.ticket.telegram.bot.service.DatabaseService;

/**
 * Сервис по работе БД
 * 
 * @author amaksimov
 */
@Service
public class DatabaseServiceImpl implements DatabaseService {

	private static final Logger LOG = LogManager.getLogger(DatabaseServiceImpl.class);

	@Autowired
	private StateRepository stateRepository;

	@Autowired
	private TaskBotRepository taskBotRepository;

	@Loggable
	@Override
	public State findByUserIdAndChatIdAndUserName(Integer userId, Long chatId, String userName) {
		State state;
		List<State> states = stateRepository.findAllByUserIdAndChatId(userId, chatId);
		if(states.isEmpty()) {
			state = new State();
			state.setUserId(userId);
			state.setChatId(chatId);
			state.setUserName(userName);
			state.setType(StateType.START_STATE);
		} else if (states.size() > 1) {
			state = states.iterator().next();
			if(StringUtils.isBlank(state.getUserName())) {
				state.setUserName(userName);
				save(state);
			} 
			LOG.warn("Found many states with userId:" + userId + " chatId: " + chatId);
		} else {
			state = states.iterator().next();
			//TODO убрать когда 2 первых пользователя будут заполнены 
			if(StringUtils.isBlank(state.getUserName())) {
				state.setUserName(userName);
				save(state);
			} 
		}
		return state;
	}

	@Override
	public State findById(Long stateId) {
		return stateRepository.findOne(stateId);
	}

	@Override
	public State save(State state) {
		return stateRepository.save(state);
	}

	@Override
	public TaskBot save(TaskBot taskBot) {
		return taskBotRepository.save(taskBot);
	}
}
