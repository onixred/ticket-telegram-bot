package ru.maksimov.andrey.ticket.telegram.bot.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.googlecode.charts4j.AxisLabels;
import com.googlecode.charts4j.AxisLabelsFactory;
import com.googlecode.charts4j.AxisStyle;
import com.googlecode.charts4j.AxisTextAlignment;
import com.googlecode.charts4j.Color;
import com.googlecode.charts4j.Data;
import com.googlecode.charts4j.Fills;
import com.googlecode.charts4j.GCharts;
import com.googlecode.charts4j.LineChart;
import com.googlecode.charts4j.LinearGradientFill;
import com.googlecode.charts4j.Plot;
import com.googlecode.charts4j.Plots;

import ru.maksimov.andrey.commons.utils.DateUtils;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.ArchiveFreePlaceDto;
import ru.maksimov.andrey.ticket.telegram.bot.service.ImageService;

/**
 * Сервис по работе с диаграммами через google api
 * 
 * @author amaksimov
 */
@Service(value = "GoogleImageService")
public class GoogleImageService implements ImageService {

	private static final Logger LOG = LogManager.getLogger(GoogleImageService.class);

	private static final String FORMAT = "&.png";

	@Override
	public List<String> getImageUrls(Map<String, Map<String, List<ArchiveFreePlaceDto>>> placeType2places,
			String title) {
		LOG.info("Start get image urls");
		List<String> urls = new ArrayList<String>();
		Date minDate = null;
		Date maxDate = null;
		Map<String, List<ArchiveFreePlaceDto>> sortedPlaceTypeAndCarName2places = new HashMap<String, List<ArchiveFreePlaceDto>>();
		for (String placeType : placeType2places.keySet()) {
			Map<String, List<ArchiveFreePlaceDto>> carName2places = placeType2places.get(placeType);
			for (String carName : carName2places.keySet()) {
				List<ArchiveFreePlaceDto> places = carName2places.get(carName);
				List<ArchiveFreePlaceDto> sortedPlaces = places.stream()
						.sorted((object1, object2) -> object1.getDateUpdate().compareTo(object2.getDateUpdate()))
						.collect(Collectors.toList());
				sortedPlaceTypeAndCarName2places.put(placeType + carName, sortedPlaces);
				if (minDate == null || sortedPlaces.get(0).getDateUpdate().before(minDate)) {
					minDate = sortedPlaces.get(0).getDateUpdate();
				}
				if (!sortedPlaces.isEmpty()) {
					if (maxDate == null || sortedPlaces.get(sortedPlaces.size() - 1).getDateUpdate().after(maxDate)) {
						maxDate = sortedPlaces.get(sortedPlaces.size() - 1).getDateUpdate();
					}
				}
			}
		}

		List<Plot> chartPrice2Time = new ArrayList<Plot>();
		List<Plot> chartFreePlaces2Time = new ArrayList<Plot>();
		Double minTariff = null;
		Double maxTariff = null;
		Integer minPlace = null;
		Integer maxPlace = null;
		List<String> times = new ArrayList<String>();
		long diff = maxDate.getTime() - minDate.getTime();
		int size = 6;
		for (int index = 0; index < size; index++) {
			long time = minDate.getTime() + (diff * index / size);
			String date = DateUtils.formatDate(new Date(time), "HH:mm dd.MM");
			times.add(date);
		}

		for (String placeTypeAndCarName : sortedPlaceTypeAndCarName2places.keySet()) {
			List<ArchiveFreePlaceDto> places = sortedPlaceTypeAndCarName2places.get(placeTypeAndCarName);
			List<Double> prices = new ArrayList<Double>();
			List<Integer> freePlaces = new ArrayList<Integer>();
			for (ArchiveFreePlaceDto place : places) {
				prices.add(place.getTariff());
				freePlaces.add(place.getFreeNumber());
				double minNew = getMin(place.getTariff(), place.getTariffAlternative());
				if (minTariff == null || minNew < minTariff) {
					minTariff = minNew;
				}
				double maxNew = getMax(place.getTariff(), place.getTariffAlternative());
				if (maxTariff == null || maxNew > maxTariff) {
					maxTariff = maxNew;
				}

				if (minPlace == null || place.getFreeNumber() < minPlace) {
					minPlace = place.getFreeNumber();
				}
				if (maxPlace == null || place.getFreeNumber() > maxPlace) {
					maxPlace = place.getFreeNumber();
				}
			}

			Random rand = new Random();
			int nextInt = rand.nextInt(256 * 256 * 256);
			String colorCode = String.format("%06x", nextInt);
			Color plotPriceColor = Color.newColor(colorCode);
			Plot plotPrice = Plots.newLine(Data.newData(prices), plotPriceColor, placeTypeAndCarName);
			chartPrice2Time.add(plotPrice);

			nextInt = rand.nextInt(256 * 256 * 256);
			colorCode = String.format("%06x", nextInt);
			Color freePlacesColor = Color.newColor(colorCode);
			Plot plotFreePlaces = Plots.newLine(Data.newData(freePlaces), freePlacesColor, placeTypeAndCarName);
			chartFreePlaces2Time.add(plotFreePlaces);
		}
		LineChart chartPrice = GCharts.newLineChart(chartPrice2Time);
		LineChart chartFreePlaces = GCharts.newLineChart(chartFreePlaces2Time);

		AxisStyle axisStyle = AxisStyle.newAxisStyle(Color.BLACK, 13, AxisTextAlignment.CENTER);
		AxisLabels axisPrice = AxisLabelsFactory.newAxisLabels("Цена", 50.0);
		axisPrice.setAxisStyle(axisStyle);
		AxisLabels axisDate = AxisLabelsFactory.newAxisLabels("Время", 50.0);
		axisDate.setAxisStyle(axisStyle);

		// Adding axis info to chart.
		chartPrice.addXAxisLabels(AxisLabelsFactory.newAxisLabels(times));

		minTariff = minTariff == null ? 0 : minTariff;
		maxTariff = maxTariff == null ? 50000 : maxTariff;
		chartPrice.addYAxisLabels(AxisLabelsFactory.newNumericRangeAxisLabels(minTariff, maxTariff));
		chartPrice.addYAxisLabels(axisPrice);
		chartPrice.addXAxisLabels(axisDate);

		chartPrice.setSize(600, 450);
		chartPrice.setTitle(title, Color.BLACK, 16);
		chartPrice.setGrid(100, 10, 3, 2);
		chartPrice.setBackgroundFill(Fills.newSolidFill(Color.ALICEBLUE));
		LinearGradientFill fill = Fills.newLinearGradientFill(0, Color.LAVENDER, 100);
		fill.addColorAndOffset(Color.WHITE, 0);
		chartPrice.setAreaFill(fill);
		urls.add(chartPrice.toURLString() + FORMAT);

		AxisLabels axisFreePlaces = AxisLabelsFactory.newAxisLabels("Места", 50.0);
		axisFreePlaces.setAxisStyle(axisStyle);

		// Adding axis info to chart.
		chartFreePlaces.addXAxisLabels(AxisLabelsFactory.newAxisLabels(times));
		minPlace = minPlace == null ? 0 : minPlace;
		maxPlace = maxPlace == null ? 50 : maxPlace;
		chartFreePlaces.addYAxisLabels(AxisLabelsFactory.newNumericRangeAxisLabels(minPlace, maxPlace));
		chartFreePlaces.addYAxisLabels(axisFreePlaces);
		chartFreePlaces.addXAxisLabels(axisDate);

		chartFreePlaces.setSize(600, 450);
		chartFreePlaces.setTitle(title, Color.BLACK, 16);
		chartFreePlaces.setGrid(100, 10, 3, 2);
		chartFreePlaces.setBackgroundFill(Fills.newSolidFill(Color.ALICEBLUE));
		chartFreePlaces.setAreaFill(fill);
		urls.add(chartFreePlaces.toURLString() + FORMAT);
		LOG.info("End get image urls");
		return urls;

	}

	private static double getMin(double value1, double value2) {
		double res;
		if (value1 < value2 || value2 == 0.0d) {
			res = value1;
		} else {
			res = value2;
		}
		return res;
	}

	private static double getMax(double value1, double value2) {
		double res;
		if (value1 > value2) {
			res = value1;
		} else {
			res = value2;
		}
		return res;
	}

}
