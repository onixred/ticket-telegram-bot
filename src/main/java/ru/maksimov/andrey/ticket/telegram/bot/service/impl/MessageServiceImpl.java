package ru.maksimov.andrey.ticket.telegram.bot.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboardRemove;

import org.telegram.telegrambots.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import com.fasterxml.jackson.core.type.TypeReference;

import ru.maksimov.andrey.commons.exception.BusinessException;
import ru.maksimov.andrey.commons.exception.SystemException;
import ru.maksimov.andrey.commons.exception.VerificationException;
import ru.maksimov.andrey.commons.log.Loggable;
import ru.maksimov.andrey.commons.utils.DateUtils;
import ru.maksimov.andrey.commons.utils.StringUtils;
import ru.maksimov.andrey.notification.ticket.purchase.client.NotificationTicketPurchaseClient;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.ArchiveTicketDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.ArchiveTicketsDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.MessageDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.StationDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.TaskDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.TimetableDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.UserDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.type.CarType;
import ru.maksimov.andrey.notification.ticket.purchase.client.type.PlaceType;
import ru.maksimov.andrey.notification.ticket.purchase.client.type.TaskStatus;
import ru.maksimov.andrey.ticket.telegram.bot.utils.Command;
import ru.maksimov.andrey.ticket.telegram.bot.utils.ConvertEntity2DtoUtil;
import ru.maksimov.andrey.ticket.telegram.bot.utils.Text;
import ru.maksimov.andrey.ticket.telegram.bot.config.Config;
import ru.maksimov.andrey.commons.utils.emoji.Emoji;
import ru.maksimov.andrey.ticket.telegram.bot.entity.State;
import ru.maksimov.andrey.ticket.telegram.bot.entity.StateType;
import ru.maksimov.andrey.ticket.telegram.bot.entity.TaskBot;
import ru.maksimov.andrey.ticket.telegram.bot.handler.HandlerLongPolling;
import ru.maksimov.andrey.ticket.telegram.bot.handler.HandlerWebhook;
import ru.maksimov.andrey.ticket.telegram.bot.service.DatabaseService;
import ru.maksimov.andrey.ticket.telegram.bot.service.MessageService;
import ru.maksimov.andrey.ticket.telegram.bot.service.TaskService;
import ru.maksimov.andrey.ticket.telegram.bot.task.TripStats;

/**
 * Сервис по работе с сообщением
 * 
 * @author amaksimov
 */
@Service
public class MessageServiceImpl implements MessageService {

	private static final Logger LOG = LogManager.getLogger(MessageServiceImpl.class);

	private static final Set<String> commandList = new HashSet<String>(
			Arrays.asList(Command.STOP_COMMAND, Command.START_COMMAND, Command.HELP_COMMAND, Command.SETTINGS_COMMAND));

	private Integer count = 0;
	private Date restartDate;

	private Config config = Config.getConfig();

	@Autowired
	private DatabaseService databaseService;

	@Autowired
	private NotificationTicketPurchaseClient notificationTicketPurchaseClient;

	@Autowired
	private HandlerWebhook handlerWebhook;

	@Autowired
	HandlerLongPolling handlerLongPolling;

	@Autowired
	private TaskService taskService;

	@Autowired
	private ApplicationContext applicationContext;

	/**
	 * Обработчик сообщения
	 * 
	 * @param message
	 *            сообщение
	 */
	@Loggable
	@Override
	public void handleIncomingMessage(Message message) throws TelegramApiException {
		State state = databaseService.findByUserIdAndChatIdAndUserName(message.getFrom().getId(), message.getChatId(),
				message.getFrom().getUserName());
		if (!message.isUserMessage() && message.hasText()) {
			// сообщения не из диалога
			if (isCommandForOther(message.getText())) {
				// Команда не для этого бота или бот не знает ее.
				return;
			} else if (message.getText().startsWith(Command.STOP_COMMAND)) {
				state.setDeleted(true);
				SendMessage sendMessage = sendHideKeyboard(state, message.getMessageId());
				sendMessage(sendMessage);
				return;
			}
		}
		// нашли глобальную команду и вышли
		if (checkHomeCommand(message, state)) {
			return;
		}
		SendMessage sendMessageRequest;
		// TODO после добавления настроек убрать условие
		if (state.isDeleted()) {
			state.setDeleted(false);
			databaseService.save(state);
		}
		switch (state.getType()) {
		case MAIN_MENU:
			sendMessageRequest = messageOnMainMenu(message, state);
			break;
		case LIST_TASK:
		case LIST_STATION_1:
		case LIST_STATION_2:
		case LIST_TIMETABLE:
			sendMessageRequest = messageOnList(message, state);
			break;
		case SELECT_TASK:
			sendMessageRequest = messageOnSelectTask(message, state);
			break;
		case SELECT_TIMETABLE:
			sendMessageRequest = messageOnSelectTimetable(message, state);
			break;
		case ADD_TASK_STATION_1:
		case ADD_TASK_STATION_2:
		case ADD_TASK_DATE_START:
		case ADD_TASK_CAR_TYPE:
		case ADD_TASK_PLACE_TYPE:
		case ADD_TASK_COUNT_PLACE:
			sendMessageRequest = messageOnAddTask(message, state);
			break;
		case ADD_TIMETABLE:
			sendMessageRequest = messageOnAddTimetable(message, state);
			break;
		case SETTINGS:
			sendMessageRequest = messageOnSetting(message, state);
			break;
		default:
			sendMessageRequest = sendMessageDefault(message, state);
			break;
		}
		sendMessage(sendMessageRequest);
	}

	@Loggable
	@Override
	public void notificationSend(MessageDto messageDto) throws VerificationException {
		State state = databaseService.findById(messageDto.getContactId());
		if (state != null) {
			if (state.isDeleted()) {
				LOG.info("Canceled notification send Bot is deleted. ContactId " + messageDto.getContactId()
						+ "\n content:" + messageDto.getContent());
				return;
			}
			SendMessage sendMessage = new SendMessage();
			sendMessage.enableMarkdown(true);
			sendMessage.setChatId(state.getChatId());
			sendMessage.setReplyMarkup(getMainMenuKeyboard());
			sendMessage.setText(messageDto.getContent());
			try {
				sendMessage(sendMessage);
			} catch (TelegramApiException tae) {
				LOG.warn("Unable send message. ContactId " + messageDto.getContactId() + " " + tae.getMessage()
						+ "\n content:" + messageDto.getContent(), tae);
				throw new VerificationException(
						"Unable send message. ContactId " + messageDto.getContactId() + " " + tae.getMessage(), tae);
			}
		} else {
			throw new VerificationException(
					"Unable send message. Status not found: сontactId " + messageDto.getContactId());
		}
	}

	/**
	 * Обработчик добавить задачи(станции, даты )
	 * 
	 * @param message
	 *            сообщение
	 * @param state
	 *            состояние
	 * @return сообщение для отправки
	 */
	private SendMessage messageOnAddTask(Message message, State state) {
		SendMessage sendMessageRequest = null;
		if (message.hasText()) {
			if (org.apache.commons.lang3.StringUtils.isNotBlank(message.getText())) {
				try {
					switch (state.getType()) {
					case ADD_TASK_STATION_1:
					case ADD_TASK_STATION_2:
						String stationName = validation(message.getText(), Text.FILTR_STATION_RUS_MESSAGE, true);
						sendMessageRequest = onAddTaskStation(message, state, stationName);
						break;
					case ADD_TASK_DATE_START:
						String date = validation(message.getText(), Text.FILTR_DATE_RUS_MESSAGE, true);
						sendMessageRequest = onAddTaskDate(message, state, date);
						break;
					case ADD_TASK_CAR_TYPE:
					case ADD_TASK_PLACE_TYPE:
						sendMessageRequest = onAddTaskType(message, state);
						break;
					case ADD_TASK_COUNT_PLACE:
						String countPlace = validation(message.getText(), Text.FILTR_DIGIT_MESSAGE, false);
						sendMessageRequest = onAddTaskCountPlace(message, state, countPlace);
						break;
					default:
						break;
					}
				} catch (VerificationException ve) {
					LOG.warn("Unable add task. ", ve);
				}

			}
		}
		if (sendMessageRequest == null) {
			sendMessageRequest = onNoFoundTask(message, state);
		}
		return sendMessageRequest;
	}

	/**
	 * Обработчик добавить времменой интервал
	 * 
	 * @param message
	 *            сообщение
	 * @param state
	 *            состояние
	 * @return сообщение для отправки
	 */
	private SendMessage messageOnAddTimetable(Message message, State state) {
		SendMessage sendMessageRequest = null;
		if (message.hasText()) {
			if (org.apache.commons.lang3.StringUtils.isNotBlank(message.getText())) {
				try {
					String time = validation(message.getText(), Text.FILTR_TIME_RUS_MESSAGE, true);
					sendMessageRequest = onAddTimetableTime(message, state, time);
				} catch (VerificationException ve) {
					LOG.warn("Unable add timetable. ", ve);
				}

			}
		}
		if (sendMessageRequest == null) {
			sendMessageRequest = onNoFoundTask(message, state);
		}
		return sendMessageRequest;
	}

	/**
	 * Обработчик меню "добавить временной интервал"
	 * 
	 * @param message
	 *            сообщение
	 * @param state
	 *            состояние
	 * @param times
	 *            временной интервал
	 * @return сообщение для отправки
	 */
	private SendMessage onAddTimetableTime(Message message, State state, String times) {
		SendMessage sendMessage = new SendMessage();
		sendMessage.enableMarkdown(true);
		ReplyKeyboardMarkup replyKeyboardMarkup = null;
		String text = null;
		try {
			TimetableDto timetable = new TimetableDto();
			String[] timesArr = times.split(" ");
			if (timesArr.length > 1) {
				Date startTime = DateUtils.parseDate(timesArr[0],
						Collections.singletonList(DateUtils.SHORT_TIME_FORMAT_RUSSIA));
				timetable.setStartTime(startTime);
				Date endTime = DateUtils.parseDate(timesArr[1],
						Collections.singletonList(DateUtils.SHORT_TIME_FORMAT_RUSSIA));
				timetable.setEndTime(endTime);
			}

			if (ConvertEntity2DtoUtil.validation(timetable)) {
				text = String.format(Text.ON_ADD_TIMETABLE_END, Emoji.WhiteHeavyCheckMark.toString());
				try {
					if (!state.isRegistered()) {
						UserDto userDto = ConvertEntity2DtoUtil.createUserDto(state.getId());
						notificationTicketPurchaseClient.update(userDto);
						state.setRegistered(true);
						databaseService.save(state);
					}
					notificationTicketPurchaseClient.update(timetable, state.getId().toString());
					state.setType(StateType.SETTINGS);
					// клавиатура меню настроек
					replyKeyboardMarkup = getSettingsKeyboard();
				} catch (SystemException e) {
					LOG.warn("Unable add Timetable " + e.getMessage(), e);
				}
			}
			databaseService.save(state);
		} catch (NumberFormatException | BusinessException e) {
			text = getResultMessage(Text.ON_ADD_TASK_ERROR_RES, e.getMessage());
		}
		if (text == null) {
			text = getResultMessage(Text.ON_ADD_TASK_ERROR_RES, " не верный формат.");
		}
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		sendMessage.setReplyToMessageId(message.getMessageId());
		sendMessage.setChatId(message.getChatId());
		sendMessage.setText(text);
		return sendMessage;
	}

	/**
	 * Обработчик "Список времменых интервалов"
	 * 
	 * @param message
	 *            сообщение
	 * @param state
	 *            состояние
	 * @return сообщение для отправки
	 */
	private SendMessage messageOnListTimetable(Message message, State state) {
		SendMessage sendMessageRequest = null;
		if (message.hasText()) {
			if (getAddTimetableCommand().equals(message.getText())) {
				// add Timetable
				sendMessageRequest = onAddTimetable(message, state);
			} else if (getBackCommand().equals(message.getText())) {
				// back
				sendMessageRequest = onSettingsChoosen(message, state);
			}

			if (sendMessageRequest == null) {
				sendMessageRequest = onNoFoundTask(message, state);
			}
		}
		return sendMessageRequest;
	}

	/**
	 * Обработчик меню "добавить станцию"
	 * 
	 * @param message
	 *            сообщение
	 * @param state
	 *            состояние
	 * @param stationName
	 *            название станции
	 * @return сообщение для отправки
	 */
	private SendMessage onAddTaskStation(Message message, State state, String stationName) {
		SendMessage sendMessage = new SendMessage();
		sendMessage.enableMarkdown(true);
		ReplyKeyboardMarkup replyKeyboardMarkup;
		String text;
		try {
			List<StationDto> station = notificationTicketPurchaseClient.findAllByName(stationName);
			replyKeyboardMarkup = getAddTaskKeyboard(state, station, stationName);
			if (state.getType().equals(StateType.ADD_TASK_STATION_1)) {
				text = Text.ON_ADD_TASK_STATION_1_LIST;
				state.setType(StateType.LIST_STATION_1);
			} else {
				text = Text.ON_ADD_TASK_STATION_2_LIST;
				state.setType(StateType.LIST_STATION_2);
			}
			databaseService.save(state);
		} catch (SystemException e) {
			replyKeyboardMarkup = getBackKeyboard();
			text = getResultMessage(Text.ON_ADD_TASK_ERROR_RES, e.getMessage());
		}
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		sendMessage.setReplyToMessageId(message.getMessageId());
		sendMessage.setChatId(message.getChatId());
		sendMessage.setText(text);
		return sendMessage;
	}

	/**
	 * Обработчик меню "добавить дату"
	 * 
	 * @param message
	 *            сообщение
	 * @param state
	 *            состояние
	 * @param data
	 *            дата
	 * @return сообщение для отправки
	 */
	private SendMessage onAddTaskDate(Message message, State state, String dateString) {
		SendMessage sendMessage = new SendMessage();
		sendMessage.enableMarkdown(true);
		ReplyKeyboardMarkup replyKeyboardMarkup;
		String text;
		try {
			Date date = DateUtils.parseDate(dateString, Collections.singletonList(DateUtils.SHORT_DATE_FORMAT_RUSSIA));
			text = Text.ON_ADD_TASK_CAR_TYPE;
			state.getTaskBot().setStartDate(date);
			state.setType(StateType.ADD_TASK_CAR_TYPE);
			replyKeyboardMarkup = getAddTaskTypeKeyboard(state, CarType.getElements());
			databaseService.save(state);
		} catch (BusinessException e) {
			replyKeyboardMarkup = null; // getBackKeyboard();
			text = getResultMessage(Text.ON_ADD_TASK_ERROR_RES, e.getMessage());
		}
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		sendMessage.setReplyToMessageId(message.getMessageId());
		sendMessage.setChatId(message.getChatId());
		sendMessage.setText(text);
		return sendMessage;
	}

	/**
	 * Обработчик меню "добавить количество мест"
	 * 
	 * @param message
	 *            сообщение
	 * @param state
	 *            состояние
	 * @param countPlaceString
	 *            количество мест
	 * @return сообщение для отправки
	 */
	private SendMessage onAddTaskCountPlace(Message message, State state, String countPlaceString) {
		SendMessage sendMessage = new SendMessage();
		sendMessage.enableMarkdown(true);
		ReplyKeyboardMarkup replyKeyboardMarkup;
		String text;
		try {
			int countPlace = Integer.parseInt(countPlaceString);
			state.getTaskBot().setCountPlace(countPlace);
			if (ConvertEntity2DtoUtil.validation(state.getTaskBot())) {
				text = String.format(Text.ON_ADD_TASK_END, Emoji.WhiteHeavyCheckMark.toString());
				state.setType(StateType.ADD_TASK_END);
				databaseService.save(state);
				TaskDto taskDto = ConvertEntity2DtoUtil.convertTaskDto2Сriterion(state.getTaskBot(),
						state.getId().toString());
				try {
					if (!state.isRegistered()) {
						UserDto userDto = ConvertEntity2DtoUtil.createUserDto(state.getId());
						notificationTicketPurchaseClient.update(userDto);
						state.setRegistered(true);
						databaseService.save(state);
					}
					notificationTicketPurchaseClient.update(taskDto);
					state.setType(StateType.MAIN_MENU);
					// клавиатура главного меню
					replyKeyboardMarkup = getMainMenuKeyboard();
				} catch (SystemException e) {
					text = Text.ON_ADD_TASK_END_FAIL;
					// клавиатура повторит попытку и в главное меню
					replyKeyboardMarkup = getAddTaskEndKeyboard();
				} catch (ResourceAccessException rae) {
					LOG.warn("Unable get update: " + rae.getMessage(), rae);
					state.setType(StateType.MAIN_MENU);
					replyKeyboardMarkup = getMainMenuKeyboard();
				}
			} else {
				text = Text.ON_ADD_TASK_END_ERROR;
				state.setType(StateType.MAIN_MENU);
				// клавиатура главное меню
				replyKeyboardMarkup = getMainMenuKeyboard();
			}
			databaseService.save(state);
		} catch (NumberFormatException e) {
			replyKeyboardMarkup = null;
			text = getResultMessage(Text.ON_ADD_TASK_ERROR_RES, e.getMessage());
		}
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		sendMessage.setReplyToMessageId(message.getMessageId());
		sendMessage.setChatId(message.getChatId());
		sendMessage.setText(text);
		return sendMessage;
	}

	/**
	 * Обработчик меню "добавить тип вагона\места"
	 * 
	 * @param message
	 *            сообщение
	 * @param state
	 *            состояние
	 * @return сообщение для отправки
	 */
	private SendMessage onAddTaskType(Message message, State state) {
		SendMessage sendMessage = new SendMessage();
		sendMessage.enableMarkdown(true);
		ReplyKeyboard replyKeyboardMarkup = null;
		String text;

		switch (state.getType()) {
		case ADD_TASK_CAR_TYPE:
			Map<String, CarType> command2CarType = Arrays.stream(CarType.values())
					.collect(Collectors.toMap(x -> x.getName().toUpperCase(), x -> x));
			command2CarType.remove(CarType.NO_DATA.getName().toUpperCase());
			command2CarType.remove(CarType.OTHER.getName().toUpperCase());
			text = handlerType(state, message.getText().toUpperCase(), command2CarType);
			if (text != null) {
				Set<String> placeTypes;
				if (state.getTaskBot() != null && state.getTaskBot().getCarType() != null) {
					placeTypes = new HashSet<String>();
					for (PlaceType placeType : state.getTaskBot().getCarType().getPlaceTypes()) {
						placeTypes.add(placeType.getMessage().toUpperCase());
					}
				} else {
					placeTypes = Arrays.stream(PlaceType.values()).map(x -> x.getMessage().toUpperCase())
							.collect(Collectors.toSet());
				}
				replyKeyboardMarkup = getAddTaskTypeKeyboard(state, placeTypes);
			}
			break;
		case ADD_TASK_PLACE_TYPE:
			PlaceType[] placeTypes;
			if (state.getTaskBot() != null && state.getTaskBot().getCarType() != null) {
				placeTypes = state.getTaskBot().getCarType().getPlaceTypes();
			} else {
				placeTypes = PlaceType.values();
			}
			Map<String, PlaceType> command2PlaceType = Arrays.stream(placeTypes)
					.collect(Collectors.toMap(x -> x.getMessage().toUpperCase(), x -> x));
			command2PlaceType.remove(PlaceType.NO_DATA.getName().toUpperCase());
			text = handlerType(state, message.getText().toUpperCase(), command2PlaceType);
			if (text != null) {
				replyKeyboardMarkup = getHomeKeyboard();
			}
			break;
		default:
			text = null;
			break;
		}
		databaseService.save(state);
		if (text == null) {
			text = String.format(Text.ON_ADD_TASK_ERROR_RES, Emoji.CrossMark.toString());
		}
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		sendMessage.setReplyToMessageId(message.getMessageId());
		sendMessage.setChatId(message.getChatId());
		sendMessage.setText(text);
		return sendMessage;
	}

	/**
	 * Обработчик поиск команды среди разных типов
	 * 
	 * @param <T>
	 *            тип enum
	 * @param state
	 *            состояние
	 * @param command
	 *            команда
	 * @return сообщение для отправки
	 */
	private <T> String handlerType(State state, String command, Map<String, T> command2enum) {
		String text = null;
		T aEnum = command2enum.get(command);
		if (aEnum != null) {
			switch (state.getType()) {
			case ADD_TASK_CAR_TYPE:
				state.getTaskBot().setCarType((CarType) aEnum);
				state.setType(StateType.ADD_TASK_PLACE_TYPE);
				text = Text.ON_ADD_TASK_PLACE_TYPE;
				break;
			case ADD_TASK_PLACE_TYPE:
				state.getTaskBot().setPlaceType((PlaceType) aEnum);
				state.setType(StateType.ADD_TASK_COUNT_PLACE);
				text = Text.ON_ADD_TASK_COUNT_PLACE;
				break;
			default:
				break;
			}
		}

		return text;
	}

	private ReplyKeyboardMarkup getAddTaskKeyboard(State state, List<StationDto> stations, String stationName) {

		Map<String, Long> command2id = new HashMap<String, Long>();
		List<KeyboardRow> keyboard = new ArrayList<>();
		filtrByStationNamePart(stations, stationName);
		int maxListMenu = stations.size() > config.getMaxListMenu() ? config.getMaxListMenu() : stations.size();
		for (int stationCounter = 0; stationCounter < maxListMenu; stationCounter++) {
			KeyboardRow row = new KeyboardRow();
			String command = stations.get(stationCounter).getName();
			row.add(command);
			command2id.put(command, stations.get(stationCounter).getId());
			keyboard.add(row);
		}
		try {
			String mapCommand2id = StringUtils.serialize(command2id);
			state.setMapCommand2id(mapCommand2id);
		} catch (VerificationException | BusinessException e) {
			LOG.warn("Unable set map command to id: " + e.getMessage(), e);
		}

		addKeyboardCommand(keyboard, getHomeCommand());
		return getKeyboard(keyboard);
	}

	/**
	 * Проверка строки
	 * 
	 * @param text
	 *            текст для проверки
	 * @param isCheckMaxMin
	 *            призак проверки на длину строки
	 * @return отфильтрованная строка
	 * @throws VerificationException
	 */
	private String validation(String text, String filtr, boolean isCheckMaxMin) throws VerificationException {
		String res = text.toUpperCase().replaceAll(filtr, "");
		if (isCheckMaxMin) {
			int maxSize = 200;
			int minSize = 3;
			if (res.length() > maxSize) {
				throw new VerificationException("Error string more than " + maxSize + " characters");
			} else if (res.length() < minSize) {
				throw new VerificationException("Error string less than " + minSize + " characters");
			}
		}
		return res;
	}

	/**
	 * Скрыть клавиатукру бота
	 * 
	 * @param userId
	 *            идентификатор пользователя
	 * @param chatId
	 *            идентификатор чата
	 * @param messageId
	 *            идентификатор сообщения
	 */
	private SendMessage sendHideKeyboard(State state, Integer messageId) throws TelegramApiException {
		SendMessage sendMessage = new SendMessage();
		sendMessage.setChatId(state.getChatId());
		sendMessage.enableMarkdown(true);
		sendMessage.setReplyToMessageId(messageId);
		sendMessage.setText(Emoji.Wave.toString());
		ReplyKeyboardRemove replyKeyboardRemove = new ReplyKeyboardRemove();
		replyKeyboardRemove.setSelective(true);
		sendMessage.setReplyMarkup(replyKeyboardRemove);
		state.setType(StateType.START_STATE);
		databaseService.save(state);
		return sendMessage;
	}

	/**
	 * Проверка команды
	 * 
	 * @param text
	 *            текст сообщения
	 * @return признак равен истине если этот текст не являеться командой для
	 *         бота или простой командой.
	 */
	private boolean isCommandForOther(String text) {
		String username = config.getUsername();
		boolean isSimpleCommand = commandList.contains(text);
		Set<String> commandForMe = new HashSet<String>();
		for (String command : commandList) {
			commandForMe.add(command + '@' + username);
		}
		boolean isCommandForMe = commandForMe.contains(text);
		return text.startsWith("/") && !isSimpleCommand && !isCommandForMe;
	}

	/**
	 * Обработчик "Настройки"
	 * 
	 * @param message
	 *            сообщение
	 * @param state
	 *            состояние
	 * @return сообщение для отправки
	 */
	private SendMessage messageOnSetting(Message message, State state) {
		SendMessage sendMessageRequest = null;
		if (message.hasText()) {
			if (message.getText().equals(getBackCommand())) {
				sendMessageRequest = sendMessageDefault(message, state);
			} else if (message.getText().equals(getListTimetableCommand())) {
				sendMessageRequest = onListTimetable(message, state);
			} else {
				sendMessageRequest = sendChooseOptionMessage(message.getChatId(), message.getMessageId(),
						getSettingsKeyboard());
			}
		}
		return sendMessageRequest;
	}

	/**
	 * Обработчик списка (задач/станций/расписания)
	 * 
	 * @param message
	 *            сообщение
	 * @param state
	 *            состояние
	 * @return сообщение для отправки
	 */
	private SendMessage messageOnList(Message message, State state) {
		SendMessage sendMessageRequest = null;
		if (message.hasText()) {
			String mapCommand2id = state.getMapCommand2id();
			Long id = findSelectCommand(mapCommand2id, message.getText());
			if (id != null) {
				switch (state.getType()) {
				case LIST_TASK:
					sendMessageRequest = onSelectTask(message, state, id);
					break;
				case LIST_STATION_1:
					onSaveStation(message, state, id, true);
					sendMessageRequest = onAddTask(message, state);
					break;
				case LIST_STATION_2:
					onSaveStation(message, state, id, false);
					sendMessageRequest = onAddTask(message, state);
					break;
				case LIST_TIMETABLE:
					sendMessageRequest = onSelectTimetable(message, state, id);
					break;
				default:
					break;
				}
			} else {
				switch (state.getType()) {
				case LIST_STATION_1:
					state.setType(StateType.ADD_TASK_STATION_1);
					sendMessageRequest = messageOnAddTask(message, state);
					break;
				case LIST_STATION_2:
					state.setType(StateType.ADD_TASK_STATION_2);
					sendMessageRequest = messageOnAddTask(message, state);
					break;
				case LIST_TIMETABLE:
					sendMessageRequest = messageOnListTimetable(message, state);
					break;
				default:
					break;
				}
			}
			if (sendMessageRequest == null) {
				sendMessageRequest = onNoFoundTask(message, state);
			}
		}
		return sendMessageRequest;
	}

	/**
	 * Обработчик меню "выбранная задача"
	 * 
	 * @param message
	 *            сообщение
	 * @param state
	 *            состояние
	 * @return сообщение для отправки
	 */
	private SendMessage messageOnSelectTask(Message message, State state) {
		SendMessage sendMessageRequest = null;
		if (message.hasText()) {
			String mapCommand2id = state.getMapCommand2id();
			Long id = findSelectCommand(mapCommand2id, message.getText());
			if (id != null) {
				if (getCancelTaskCommand().equals(message.getText())) {
					// cancel Task
					sendMessageRequest = onCancelTask(message, state, id);
				} else if (getInfoTaskCommand().equals(message.getText())) {
					// info Task
					sendMessageRequest = onInfoTask(message, state, id);
				}
			}
			if (sendMessageRequest == null) {
				sendMessageRequest = onNoFoundTask(message, state);
			}
		}
		return sendMessageRequest;
	}

	/**
	 * Обработчик меню "выбранный интервал оповещения"
	 * 
	 * @param message
	 *            сообщение
	 * @param state
	 *            состояние
	 * @return сообщение для отправки
	 */
	private SendMessage messageOnSelectTimetable(Message message, State state) {
		SendMessage sendMessageRequest = null;
		if (message.hasText()) {
			String mapCommand2id = state.getMapCommand2id();
			Long id = findSelectCommand(mapCommand2id, message.getText());
			if (id != null) {
				if (getCancelTimetableCommand().equals(message.getText())) {
					// cancel Timetable
					sendMessageRequest = onCancelTimetable(message, state, id);
				}
			}
			if (getBackCommand().equals(message.getText())) {
				// Back
				sendMessageRequest = onListTimetable(message, state);
			}
			if (sendMessageRequest == null) {
				sendMessageRequest = onNoFoundTask(message, state);
			}
		}
		return sendMessageRequest;
	}

	/**
	 * Обработчик меню "отменить задача"
	 * 
	 * @param message
	 *            сообщение
	 * @param state
	 *            состояние
	 * @param id
	 *            идентификатор задачи
	 * @return сообщение для отправки
	 */
	private SendMessage onCancelTask(Message message, State state, Long id) {
		String error = null;
		try {
			notificationTicketPurchaseClient.cancelById(id);
		} catch (SystemException e) {
			error = e.getMessage();
		}
		SendMessage sendMessage = new SendMessage();
		sendMessage.enableMarkdown(true);
		ReplyKeyboardMarkup replyKeyboardMarkup = getBackKeyboard();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		sendMessage.setReplyToMessageId(message.getMessageId());
		sendMessage.setChatId(message.getChatId());
		sendMessage.setText(getResultMessage(Text.ON_CANCEL_TASK, error));
		state.setType(StateType.SELECT_TASK);
		databaseService.save(state);
		return sendMessage;
	}

	/**
	 * Обработчик меню "удалить интервал оповещения"
	 * 
	 * @param message
	 *            сообщение
	 * @param state
	 *            состояние
	 * @param id
	 *            идентификатор задачи
	 * @return сообщение для отправки
	 */
	private SendMessage onCancelTimetable(Message message, State state, Long id) {
		String error = null;
		try {
			notificationTicketPurchaseClient.deleteById(id);
		} catch (SystemException e) {
			error = e.getMessage();
		}
		SendMessage sendMessage = new SendMessage();
		sendMessage.enableMarkdown(true);
		ReplyKeyboardMarkup replyKeyboardMarkup = getBackKeyboard();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		sendMessage.setReplyToMessageId(message.getMessageId());
		sendMessage.setChatId(message.getChatId());
		sendMessage.setText(getResultMessage(Text.ON_DELETE_TIMETABLE, error));
		state.setMapCommand2id(null);
		state.setType(StateType.SELECT_TIMETABLE);
		databaseService.save(state);
		return sendMessage;
	}

	/**
	 * Обработчик меню "информация по задаче"
	 * 
	 * @param message
	 *            сообщение
	 * @param state
	 *            состояние
	 * @param id
	 *            идентификатор задачи
	 * @return сообщение для отправки
	 */
	private SendMessage onInfoTask(Message message, State state, Long id) {
		String error = null;
		List<String> messages = null;
		try {
			messages = notificationTicketPurchaseClient.findByTaskId(id);
		} catch (SystemException e) {
			error = e.getMessage();
		}
		if ((messages == null || messages.isEmpty()) && error == null) {
			messages = Collections.<String> singletonList("Нет данных по задаче. Попробуйте позже.");
		}

		SendMessage sendMessage = new SendMessage();
		sendMessage.enableMarkdown(true);
		ReplyKeyboardMarkup replyKeyboardMarkup = getBackKeyboard();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		sendMessage.setChatId(message.getChatId());

		int maxSendingMesasage = messages.size();
		if (messages.size() > config.getMaxSendingMesasage()) {
			maxSendingMesasage = config.getMaxSendingMesasage();
			sendMessage.setText((String.format(Text.WARN_LIMIT_MESSAGE, Emoji.Warning, messages.size(),
					config.getMaxSendingMesasage())));
			try {
				sendMessage(sendMessage);
			} catch (TelegramApiException tae) {
				LOG.warn("Unable send warn mesages UserId:" + state.getUserId() + " error: " + tae.getMessage(), tae);
			}
		}
		for (int messageCounter = 0; messageCounter < maxSendingMesasage; messageCounter++) {
			sendMessage.setText(messages.get(messageCounter));
			try {
				sendMessage(sendMessage);
			} catch (TelegramApiException tae) {
				LOG.warn("Unable send mesages UserId:" + state.getUserId() + " error: " + tae.getMessage(), tae);
			}
		}
		sendMessage.setReplyToMessageId(message.getMessageId());
		String text = getResultMessage(Text.ON_INFO_TASK, error, Text.SUCCESS_MESSAGE);
		sendMessage.setText(text);
		state.setType(StateType.SELECT_TASK);
		databaseService.save(state);
		return sendMessage;
	}

	/**
	 * Получить клавиатуру с командой назад
	 * 
	 * @return клавиатура
	 */
	private ReplyKeyboardMarkup getBackKeyboard() {
		return getKeyboard(getBackCommand());
	}

	/**
	 * Получить клавиатуру с командой "Главное меню"
	 * 
	 * @return клавиатура
	 */
	private ReplyKeyboardMarkup getHomeKeyboard() {
		return getKeyboard(getHomeCommand());
	}

	/**
	 * Получить клавиатуру с заданным текстом
	 * 
	 * @return клавиатура
	 */
	private static ReplyKeyboardMarkup getKeyboard(String command) {
		List<KeyboardRow> keyboard = new ArrayList<>();
		addKeyboardCommand(keyboard, command);
		return getKeyboard(keyboard);
	}

	/**
	 * Получить клавиатуру с заданными кнопками
	 * 
	 * @return клавиатура
	 */
	private static ReplyKeyboardMarkup getKeyboard(List<KeyboardRow> keyboard) {
		ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
		replyKeyboardMarkup.setSelective(true);
		replyKeyboardMarkup.setResizeKeyboard(true);
		replyKeyboardMarkup.setOneTimeKeyboard(true);
		replyKeyboardMarkup.setKeyboard(keyboard);
		return replyKeyboardMarkup;
	}

	/**
	 * Найти выбранеую команду (кнопку)
	 * 
	 * @param mapCommand2id
	 *            строка сериализованной кары key - comand
	 * @param text
	 *            текст кнопки
	 * @return идентификатор кнопки
	 */
	private Long findSelectCommand(String mapCommand2id, String text) {
		Long id = null;
		if (mapCommand2id != null && !mapCommand2id.isEmpty()) {
			try {
				TypeReference<Map<String, Long>> mapType = new TypeReference<Map<String, Long>>() {
				};
				Map<String, Long> command2id = StringUtils.deserialize(mapCommand2id, mapType);
				id = command2id.get(text);
			} catch (VerificationException | BusinessException e) {
				LOG.warn("Unable get map command to id: " + e.getMessage(), e);
			}
		}
		return id;
	}

	/**
	 * Обработчик меню "выбранная задача"
	 * 
	 * @param message
	 *            сообщение
	 * @param state
	 *            состояние
	 * @param id
	 *            идентификатор задачи
	 * @return сообщение для отправки
	 */
	private SendMessage onSelectTask(Message message, State state, Long id) {
		ReplyKeyboardMarkup replyKeyboardMarkup;
		SendMessage sendMessage = new SendMessage();
		try {
			TaskDto task = notificationTicketPurchaseClient.findById(id);
			replyKeyboardMarkup = getSelectTaskKeyboard(task.getTaskId(), state);
			sendMessage.setText(getSelectTaskMessage(task));
		} catch (SystemException e) {
			LOG.warn("Unable find task by id " + e.getMessage(), e);
			replyKeyboardMarkup = null;
			sendMessage.setText(getResultMessage(Text.ON_ADD_TASK_ERROR_RES, e.getMessage()));
		}
		sendMessage.enableMarkdown(true);
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		sendMessage.setReplyToMessageId(message.getMessageId());
		sendMessage.setChatId(message.getChatId());
		state.setType(StateType.SELECT_TASK);
		databaseService.save(state);
		return sendMessage;
	}

	/**
	 * Обработчик меню "выбранное расписание"
	 * 
	 * @param message
	 *            сообщение
	 * @param state
	 *            состояние
	 * @param id
	 *            идентификатор расписания
	 * @return сообщение для отправки
	 */
	private SendMessage onSelectTimetable(Message message, State state, Long id) {
		SendMessage sendMessage = new SendMessage();
		sendMessage.enableMarkdown(true);
		ReplyKeyboardMarkup replyKeyboardMarkup = getSelectTimetableKeyboard(id, state);
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		sendMessage.setReplyToMessageId(message.getMessageId());
		sendMessage.setChatId(message.getChatId());
		sendMessage.setText(Text.CHOOSE_OPTION);
		state.setType(StateType.SELECT_TIMETABLE);
		databaseService.save(state);
		return sendMessage;
	}

	/**
	 * Сохранить станцию в задаче для бота
	 * 
	 * @param message
	 *            сообщение
	 * @param state
	 *            состояние
	 * @param id
	 *            идентификатор станции
	 * @return сообщение для отправки
	 */
	private void onSaveStation(Message message, State state, Long id, boolean isStartStation) {
		TaskBot taskBot = state.getTaskBot();
		if (taskBot == null) {
			taskBot = new TaskBot();
			taskBot = databaseService.save(taskBot);
			state.setTaskBot(taskBot);
		}
		if (isStartStation) {
			taskBot.setStationStartId(id);
		} else {
			taskBot.setStationEndId(id);
		}
		databaseService.save(state);
	}

	/**
	 * Получить клавиатуру "выбранная задача"
	 * 
	 * @param tasks
	 *            список задач
	 * @param state
	 *            состояние
	 * @return клавиатура
	 */
	private ReplyKeyboardMarkup getSelectTaskKeyboard(Long taskId, State state) {
		List<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();
		String cancelCommand = getCancelTaskCommand();
		keyboardFirstRow.add(cancelCommand);
		KeyboardRow keyboardSecondRow = new KeyboardRow();
		String infoTaskCommand = getInfoTaskCommand();
		keyboardSecondRow.add(infoTaskCommand);
		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);
		Map<String, Long> command2id = new HashMap<String, Long>();
		command2id.put(cancelCommand, taskId);
		command2id.put(infoTaskCommand, taskId);
		try {
			String mapCommand2id = StringUtils.serialize(command2id);
			state.setMapCommand2id(mapCommand2id);
		} catch (VerificationException | BusinessException e) {
			LOG.warn("Unable set map command to id: " + e.getMessage(), e);
		}
		addKeyboardCommand(keyboard, getBackCommand());
		return getKeyboard(keyboard);
	}

	/**
	 * Получить клавиатуру "выбранный интервал оповещения"
	 * 
	 * @param timetableId
	 *            идентификатор интервала оповещения
	 * @param state
	 *            состояние
	 * @return клавиатура
	 */
	private ReplyKeyboardMarkup getSelectTimetableKeyboard(Long timetableId, State state) {
		List<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();
		String cancelCommand = getCancelTimetableCommand();
		keyboardFirstRow.add(cancelCommand);
		keyboard.add(keyboardFirstRow);
		Map<String, Long> command2id = new HashMap<String, Long>();
		command2id.put(cancelCommand, timetableId);
		try {
			String mapCommand2id = StringUtils.serialize(command2id);
			state.setMapCommand2id(mapCommand2id);
		} catch (VerificationException | BusinessException e) {
			LOG.warn("Unable set map command to id: " + e.getMessage(), e);
		}
		addKeyboardCommand(keyboard, getBackCommand());
		return getKeyboard(keyboard);
	}

	/**
	 * Обработчик для случая если не найдена задача
	 * 
	 * @param message
	 *            сообщение
	 * @param state
	 *            состояние
	 * @return сообщение для отправки
	 */
	private SendMessage onNoFoundTask(Message message, State state) {
		state.setType(StateType.MAIN_MENU);
		state.setMapCommand2id(null);
		databaseService.save(state);

		SendMessage sendMessageRequest = new SendMessage();
		sendMessageRequest.enableMarkdown(true);
		sendMessageRequest.setReplyMarkup(getMainMenuKeyboard());
		sendMessageRequest.setReplyToMessageId(message.getMessageId());
		sendMessageRequest.setText("Не найдена задача");
		sendMessageRequest.setChatId(message.getChatId().toString());
		return sendMessageRequest;
	}

	/**
	 * Обработчик главного меню
	 * 
	 * @param message
	 *            сообщение
	 * @param state
	 *            состояние
	 * @return сообщение для отправки
	 */
	private SendMessage messageOnMainMenu(Message message, State state) {
		SendMessage sendMessageRequest;
		if (message.hasText()) {
			if (message.getText().equals(getListTaskCommand())) {
				sendMessageRequest = onListTask(message, state);
			} else if (message.getText().equals(getAddTaskCommand())) {
				sendMessageRequest = onAddTask(message, state);
			} else if (message.getText().equals(getSettingsCommand())) {
				sendMessageRequest = onSettingsChoosen(message, state);
			} else if (message.getText().equals(getRateCommand())) {
				sendMessageRequest = sendRateMessage(message.getChatId(), message.getMessageId(), null);
			} else if (message.getText().equals(getBugCommand())) {
				sendMessageRequest = sendBugMessage(message.getChatId(), message.getMessageId(), null);
			} else {
				sendMessageRequest = sendChooseOptionMessage(message.getChatId(), message.getMessageId(),
						getMainMenuKeyboard());
			}
		} else {
			sendMessageRequest = sendChooseOptionMessage(message.getChatId(), message.getMessageId(),
					getMainMenuKeyboard());
		}

		return sendMessageRequest;
	}

	/**
	 * Обработчик меню "настройки"
	 * 
	 * @param message
	 *            сообщение
	 * @param state
	 *            состояние
	 * @return сообщение для отправки
	 */
	private SendMessage onSettingsChoosen(Message message, State state) {
		SendMessage sendMessage = new SendMessage();
		sendMessage.enableMarkdown(true);
		ReplyKeyboardMarkup replyKeyboardMarkup = getSettingsKeyboard();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		sendMessage.setReplyToMessageId(message.getMessageId());
		sendMessage.setChatId(message.getChatId());
		sendMessage.setText(getSettingsMessage());
		state.setType(StateType.SETTINGS);
		databaseService.save(state);
		return sendMessage;
	}

	/**
	 * Обработчик меню "добавить станцию\дату"
	 * 
	 * @param message
	 *            сообщение
	 * @param state
	 *            состояние
	 * @return сообщение для отправки
	 */
	private SendMessage onAddTask(Message message, State state) {
		SendMessage sendMessage = new SendMessage();
		sendMessage.enableMarkdown(true);
		ReplyKeyboardMarkup replyKeyboardMarkup = getAddTaskKeyboard();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		sendMessage.setReplyToMessageId(message.getMessageId());
		sendMessage.setChatId(message.getChatId());
		switch (state.getType()) {
		case LIST_STATION_1:
			sendMessage.setText(Text.ON_ADD_TASK_STATION_2);
			state.setType(StateType.ADD_TASK_STATION_2);
			break;
		case LIST_STATION_2:
			sendMessage.setText(Text.ON_ADD_TASK_DATE_START);
			state.setType(StateType.ADD_TASK_DATE_START);
			break;
		default:
			Integer countTask = null;
			int tasksLimit = config.getTasksLimit();
			try {
				countTask = notificationTicketPurchaseClient.getCountTaskByStatus(TaskStatus.NEW);
			} catch (SystemException se) {
				LOG.warn("Unable get count tasks by status. Message: " + se.getMessage(), se);
				String errorMessage = getResultMessage(Text.ON_ADD_TASK_ERROR_RES, se.getMessage());
				sendMessage.setText(errorMessage);
				return sendMessage;
			}
			if (countTask >= tasksLimit) {
				sendMessage.setText(String.format(Text.ON_ADD_TASKS_LIMIT, Emoji.CrossMark));
			} else {
				countTask = null;
				try {
					countTask = notificationTicketPurchaseClient.getCountTaskByStatusAndLogin(TaskStatus.NEW,
							state.getId().toString());
				} catch (SystemException se) {
					LOG.warn("Unable get count tasks by status and login. Message: " + se.getMessage(), se);
				}
				int limit = config.getTasksLimitForUser();
				boolean isIgnore = config.getTasksLimitIgnoreUsers().contains(state.getUserName());
				if ((countTask == null || countTask >= limit) && !isIgnore) {
					sendMessage.setText(String.format(Text.ON_ADD_TASKS_LIMIT_FOR_USER, Emoji.CrossMark, limit));
				} else {
					sendMessage.setText(Text.ON_ADD_TASK_STATION_1);
					state.setType(StateType.ADD_TASK_STATION_1);
				}
			}
			break;
		}

		databaseService.save(state);
		return sendMessage;
	}

	private SendMessage onAddTimetable(Message message, State state) {
		SendMessage sendMessage = new SendMessage();
		sendMessage.enableMarkdown(true);
		ReplyKeyboardMarkup replyKeyboardMarkup = getHomeKeyboard();
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		sendMessage.setReplyToMessageId(message.getMessageId());
		sendMessage.setChatId(message.getChatId());

		Integer countTask = null;
		try {
			countTask = notificationTicketPurchaseClient.getCountTimetableByLogin(state.getId().toString());
		} catch (SystemException e) {
			LOG.warn("Unable get count Timetable by login " + state.getId() + " Msg: " + e.getMessage(), e);
		}
		int limit = config.getTimetableLimit();
		if (countTask == null || countTask >= limit) {
			sendMessage.setText(String.format(Text.ON_ADD_TIMETABLE_LIMIT, Emoji.CrossMark, limit));
		} else {
			sendMessage.setText(Text.ON_ADD_TIMETABLE);
			state.setType(StateType.ADD_TIMETABLE);
		}

		databaseService.save(state);
		return sendMessage;
	}

	/**
	 * Обработчик меню "список задач"
	 * 
	 * @param message
	 *            сообщение
	 * @param state
	 *            состояние
	 * @return сообщение для отправки
	 */
	private SendMessage onListTask(Message message, State state) {
		List<TaskDto> tasks = Collections.<TaskDto> emptyList();
		try {
			tasks = notificationTicketPurchaseClient.findAllByStatusAndLogin(TaskStatus.NEW, state.getId().toString());
		} catch (SystemException se) {
			LOG.warn("Unable get list timetable " + se.getMessage(), se);
			tasks = Collections.<TaskDto> emptyList();
		}
		SendMessage sendMessage = new SendMessage();
		sendMessage.enableMarkdown(true);
		ReplyKeyboardMarkup replyKeyboardMarkup = getListTaskKeyboard(message.getFrom().getId(), tasks, state);
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		sendMessage.setReplyToMessageId(message.getMessageId());
		sendMessage.setChatId(message.getChatId());
		sendMessage.setText(Text.ON_LIST_TASK);
		state.setType(StateType.LIST_TASK);
		databaseService.save(state);
		return sendMessage;
	}

	/**
	 * Обработчик меню "список расписаний"
	 * 
	 * @param message
	 *            сообщение
	 * @param state
	 *            состояние
	 * @return сообщение для отправки
	 */
	private SendMessage onListTimetable(Message message, State state) {
		List<TimetableDto> timetables = Collections.<TimetableDto> emptyList();
		try {
			timetables = notificationTicketPurchaseClient.findAllByLogin(state.getId().toString());
		} catch (SystemException se) {
			LOG.warn("Unable get list timetable " + se.getMessage(), se);
		}

		SendMessage sendMessage = new SendMessage();
		sendMessage.enableMarkdown(true);
		ReplyKeyboardMarkup replyKeyboardMarkup = getListTimetableKeyboard(message.getFrom().getId(), timetables,
				state);
		sendMessage.setReplyMarkup(replyKeyboardMarkup);
		sendMessage.setReplyToMessageId(message.getMessageId());
		sendMessage.setChatId(message.getChatId());
		sendMessage.setText(Text.ON_LIST_TIMETABLE);
		state.setType(StateType.LIST_TIMETABLE);
		databaseService.save(state);
		return sendMessage;
	}

	/**
	 * Получить клавиатуру список задач
	 * 
	 * @param id
	 *            идентификатор сообщения
	 * @param tasks
	 *            список задач
	 * @param state
	 *            состояние
	 * @return клавиатура
	 */
	private ReplyKeyboardMarkup getListTaskKeyboard(Integer id, List<TaskDto> tasks, State state) {
		Map<String, Long> command2id = new HashMap<String, Long>();
		List<KeyboardRow> keyboard = new ArrayList<>();
		for (TaskDto task : tasks) {
			KeyboardRow row = new KeyboardRow();
			String command = getTaskCommand(task);
			row.add(command);
			command2id.put(command, task.getTaskId());
			keyboard.add(row);
		}
		try {
			String mapCommand2id = StringUtils.serialize(command2id);
			state.setMapCommand2id(mapCommand2id);
		} catch (VerificationException | BusinessException e) {
			LOG.warn("Unable set map command to id: " + e.getMessage(), e);
		}

		addKeyboardCommand(keyboard, getBackCommand());
		return getKeyboard(keyboard);
	}

	/**
	 * Получить клавиатуру список расписаний
	 * 
	 * @param id
	 *            идентификатор сообщения
	 * @param timetables
	 *            список расписаний
	 * @param state
	 *            состояние
	 * @return клавиатура
	 */
	private ReplyKeyboardMarkup getListTimetableKeyboard(Integer id, List<TimetableDto> timetables, State state) {
		Map<String, Long> command2id = new HashMap<String, Long>();
		List<KeyboardRow> keyboard = new ArrayList<>();
		for (TimetableDto timetable : timetables) {
			KeyboardRow row = new KeyboardRow();
			String command = getTimetableCommand(timetable);
			row.add(command);
			command2id.put(command, timetable.getId());
			keyboard.add(row);
		}
		try {
			String mapCommand2id = StringUtils.serialize(command2id);
			state.setMapCommand2id(mapCommand2id);
		} catch (VerificationException | BusinessException e) {
			LOG.warn("Unable set map command to id: " + e.getMessage(), e);
		}
		addKeyboardCommand(keyboard, getAddTimetableCommand());
		addKeyboardCommand(keyboard, getBackCommand());
		return getKeyboard(keyboard);
	}

	/**
	 * Добавить команду
	 * 
	 * @param keyboard
	 *            клавиатура
	 * @param command
	 *            команда
	 */
	private static void addKeyboardCommand(List<KeyboardRow> keyboard, String command) {
		KeyboardRow row = new KeyboardRow();
		row.add(command);
		keyboard.add(row);
	}

	/**
	 * Получить сообщение "настройки"
	 * 
	 * @return сообщение
	 */
	private static String getSettingsMessage() {
		return String.format(Text.ON_SETTINGS_COMMAND, Emoji.Back, Emoji.Watch);
	}

	/**
	 * Получить сообщение "помощь"
	 * 
	 * @return сообщение
	 */
	private static String getHelpMessage() {
		return String.format(Text.HELP_WEATHER_MESSAGE, Emoji.Clipboard.toString(), Emoji.HeavyPlusSign.toString(),
				Emoji.Wrench.toString(), Emoji.Top.toString());
	}

	/**
	 * Получить сообщение "оцени бота"
	 * 
	 * @return сообщение
	 */
	private String getRateMessage() {
		String botName = config.getUsername();
		return String.format(Text.RATE_ME_MESSAGE, botName);
	}

	/**
	 * Получить сообщение "сообщить о баге"
	 * 
	 * @return сообщение
	 */
	private String getBugMessage() {
		return String.format(Text.BUG_INFO_MESSAGE, Emoji.Bug.toString());
	}

	/**
	 * Получить сообщение об ошибке или успехе
	 * 
	 * @param msgBase
	 *            Основноее сообщение
	 * @param error
	 *            ошибка
	 * @param success
	 *            сообщение при успехе
	 * @return сообщение
	 */
	private String getResultMessage(String msgBase, String error, String success) {
		String message;
		Emoji emoji;
		if (error == null) {
			emoji = Emoji.WhiteCheckMark;
			message = success;
		} else {
			emoji = Emoji.CrossMark;
			message = error;
		}
		return String.format(msgBase + message, emoji.toString());
	}

	/**
	 * Получить сообщение об ошибке или успехе
	 * 
	 * @param msgBase
	 *            Основноее сообщение
	 * @param error
	 *            ошибка
	 * @return сообщение
	 */
	private String getResultMessage(String msgBase, String error) {
		return getResultMessage(msgBase, error, Text.SUCCESS_MESSAGE);
	}

	/**
	 * Получить клавиатуру основное меню
	 * 
	 * @return клавиатура
	 */
	private static ReplyKeyboardMarkup getMainMenuKeyboard() {
		List<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();
		keyboardFirstRow.add(getListTaskCommand());
		keyboardFirstRow.add(getAddTaskCommand());
		KeyboardRow keyboardSecondRow = new KeyboardRow();
		keyboardSecondRow.add(getSettingsCommand());
		keyboardSecondRow.add(getRateCommand());
		KeyboardRow keyboardThirdRow = new KeyboardRow();
		keyboardThirdRow.add(getBugCommand());
		keyboard.add(keyboardFirstRow);
		keyboard.add(keyboardSecondRow);
		keyboard.add(keyboardThirdRow);
		return getKeyboard(keyboard);
	}

	/**
	 * Получить клавиатуру "Настройки"
	 * 
	 * @return клавиатура
	 */
	private static ReplyKeyboardMarkup getSettingsKeyboard() {
		List<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardFirstRow = new KeyboardRow();
		keyboardFirstRow.add(getListTimetableCommand());
		keyboard.add(keyboardFirstRow);
		addKeyboardCommand(keyboard, getBackCommand());
		return getKeyboard(keyboard);
	}

	/**
	 * Получить клавиатуру "Типов"
	 * 
	 * @return клавиатура
	 */
	private ReplyKeyboardMarkup getAddTaskTypeKeyboard(State state, Set<String> types) {
		List<KeyboardRow> keyboard = new ArrayList<>();
		// общая часть
		for (String type : types) {
			KeyboardRow keyboardRow = new KeyboardRow();
			keyboardRow.add(type);
			keyboard.add(keyboardRow);
		}
		addKeyboardCommand(keyboard, getHomeCommand());
		return getKeyboard(keyboard);
	}

	/**
	 * Получить клавиатуру "Повтор и Главное меню"
	 * 
	 * @return клавиатура
	 */
	private ReplyKeyboardMarkup getAddTaskEndKeyboard() {
		List<KeyboardRow> keyboard = new ArrayList<>();
		KeyboardRow keyboardRowRepeat = new KeyboardRow();
		keyboardRowRepeat.add(getRepeatCommand());
		keyboard.add(keyboardRowRepeat);
		KeyboardRow keyboardRow = new KeyboardRow();
		keyboardRow.add(getHomeCommand());
		keyboard.add(keyboardRow);
		return getKeyboard(keyboard);
	}

	/**
	 * Получить клавиатуру добавить задачу
	 * 
	 * @return клавиатура
	 */
	private ReplyKeyboardMarkup getAddTaskKeyboard() {
		return getKeyboard(getHomeCommand());
	}

	private static String getRateCommand() {
		return String.format("%sОценить бота", Emoji.Top);
	}

	private static String getBugCommand() {
		return String.format("%sБаг", Emoji.Bug);
	}

	private static String getBackCommand() {
		return String.format("%sНазад", Emoji.Back);
	}

	private static String getRepeatCommand() {
		return String.format("%sПовторить", Emoji.Repeat);
	}

	private static String getHomeCommand() {
		return String.format("%sГлавное меню", Emoji.House);
	}

	/**
	 * Получить команду "Задачи"
	 */
	private String getTaskCommand(TaskDto task) {
		String date = DateUtils.formatDate(task.getStartDate(), DateUtils.SHORT_DATE_FORMAT_RUSSIA);
		StringBuilder trip = new StringBuilder();
		trip.append("%s");
		String fromStation = StringUtils.firstSubstring(task.getStationStartName(), ",");
		trip.append(fromStation);
		trip.append("->");
		String whereStation = StringUtils.firstSubstring(task.getStationEndName(), ",");
		trip.append(whereStation);
		trip.append(" ");
		trip.append(date);
		return String.format(trip.toString(), Emoji.Train);
	}

	/**
	 * Получить команду "Интервал оповещений"
	 */
	private String getTimetableCommand(TimetableDto timetable) {
		String startTime = DateUtils.formatDate(timetable.getStartTime(), DateUtils.SHORT_TIME_FORMAT_RUSSIA,
				TimeZone.getTimeZone("GMT"));
		String endTime = DateUtils.formatDate(timetable.getEndTime(), DateUtils.SHORT_TIME_FORMAT_RUSSIA,
				TimeZone.getTimeZone("GMT"));
		StringBuilder timetableStr = new StringBuilder();
		timetableStr.append("%s");
		timetableStr.append(startTime);
		timetableStr.append("->");
		timetableStr.append(endTime);
		return String.format(timetableStr.toString(), Emoji.AlarmClock);
	}

	private static String getSettingsCommand() {
		return String.format("%sНастройки", Emoji.Wrench);
	}

	private static String getListTaskCommand() {
		return String.format("%sСписок задач", Emoji.Clipboard);
	}

	private static String getAddTaskCommand() {
		return String.format("%sДобавить задачу", Emoji.HeavyPlusSign);
	}

	private static String getCancelTaskCommand() {
		return String.format("%sОтменить задачу", Emoji.HeavyMinusSign);
	}

	/**
	 * Получить сообщение "Расписание оповещений"
	 */
	private static String getListTimetableCommand() {
		return String.format("%sРасписание оповещений", Emoji.Clipboard);
	}

	private static String getCancelTimetableCommand() {
		return String.format("%sУдалить интервал", Emoji.HeavyMinusSign);
	}

	/**
	 * Получить сообщение "Добавить интервал"
	 */
	private static String getAddTimetableCommand() {
		return String.format("%sДобавить интервал", Emoji.HeavyPlusSign);
	}

	private String getSelectTaskMessage(TaskDto task) {
		StringBuilder message = new StringBuilder();
		message.append("%s");
		message.append("Информация о задаче: \n");
		message.append(task.getStationStartName());
		message.append(" -> ");
		message.append(task.getStationEndName());
		message.append(" дата отправления: ");
		String date = DateUtils.formatDate(task.getStartDate(), DateUtils.SHORT_DATE_FORMAT_RUSSIA);
		message.append(date);
		message.append("\n Критерии поиска: ");
		message.append("\n\t Тип вагона: ");
		message.append(task.getCarType());
		message.append("\n\t Тип места: ");
		message.append(task.getPlaceType());
		message.append("\n\t Количество свободных мест: ");
		message.append(task.getCountPlace());
		message.append("\n\n");
		message.append(Text.ON_SELECT_TASK);
		return String.format(message.toString(), Emoji.Train, Emoji.CrossMark, Emoji.Back);
	}

	/**
	 * Получить сообщение "Информация о местах"
	 */
	private static String getInfoTaskCommand() {
		return String.format("%sИнформация о местах", Emoji.InformationSource);
	}

	/**
	 * Получить сообщение по умолчанию для отправки
	 * 
	 * @param message
	 *            Сообщение
	 * @param state
	 *            Состояние
	 * @return сообщение
	 */
	private SendMessage sendMessageDefault(Message message, State state) {
		ReplyKeyboardMarkup replyKeyboardMarkup = getMainMenuKeyboard();
		state.setType(StateType.MAIN_MENU);
		databaseService.save(state);
		return sendHelpMessage(message.getChatId(), message.getMessageId(), replyKeyboardMarkup);
	}

	/**
	 * Получить сообщение "выбор опции" для отправки
	 * 
	 * @param chatId
	 *            идентификатор чата
	 * @param messageId
	 *            идентификатор сообщения
	 * @param replyKeyboard
	 *            клавиатура для ответа
	 * @return сообщение
	 */
	private static SendMessage sendChooseOptionMessage(Long chatId, Integer messageId, ReplyKeyboard replyKeyboard) {
		SendMessage sendMessage = new SendMessage();
		sendMessage.enableMarkdown(true);
		sendMessage.setChatId(chatId.toString());
		sendMessage.setReplyToMessageId(messageId);
		sendMessage.setReplyMarkup(replyKeyboard);
		sendMessage.setText(Text.CHOOSE_OPTION);
		return sendMessage;
	}

	/**
	 * Получить сообщение "помощь" для отправки
	 * 
	 * @param chatId
	 *            идентификатор чата
	 * @param messageId
	 *            идентификатор сообщения
	 * @param replyKeyboard
	 *            клавиатура для ответа
	 * @return сообщение
	 */
	private static SendMessage sendHelpMessage(Long chatId, Integer messageId,
			ReplyKeyboardMarkup replyKeyboardMarkup) {
		SendMessage sendMessage = new SendMessage();
		sendMessage.enableMarkdown(true);
		sendMessage.setChatId(chatId);
		sendMessage.setReplyToMessageId(messageId);
		if (replyKeyboardMarkup != null) {
			sendMessage.setReplyMarkup(replyKeyboardMarkup);
		}
		sendMessage.setText(getHelpMessage());
		return sendMessage;
	}

	/**
	 * Получить сообщение "оцени бота" для отправки
	 * 
	 * @param chatId
	 *            идентификатор чата
	 * @param messageId
	 *            идентификатор сообщения
	 * @param replyKeyboard
	 *            клавиатура для ответа
	 * @return сообщение
	 */
	private SendMessage sendRateMessage(Long chatId, Integer messageId, ReplyKeyboardMarkup replyKeyboardMarkup) {
		SendMessage sendMessage = new SendMessage();
		sendMessage.enableMarkdown(true);
		sendMessage.setChatId(chatId);
		sendMessage.setReplyToMessageId(messageId);
		if (replyKeyboardMarkup != null) {
			sendMessage.setReplyMarkup(replyKeyboardMarkup);
		}
		sendMessage.setText(getRateMessage());
		return sendMessage;
	}

	/**
	 * Получить сообщение "Информация о баге" для отправки
	 * 
	 * @param chatId
	 *            идентификатор чата
	 * @param messageId
	 *            идентификатор сообщения
	 * @param replyKeyboard
	 *            клавиатура для ответа
	 * @return сообщение
	 */
	private SendMessage sendBugMessage(Long chatId, Integer messageId, ReplyKeyboardMarkup replyKeyboardMarkup) {
		SendMessage sendMessage = new SendMessage();
		sendMessage.enableMarkdown(true);
		sendMessage.setChatId(chatId);
		sendMessage.setReplyToMessageId(messageId);
		if (replyKeyboardMarkup != null) {
			sendMessage.setReplyMarkup(replyKeyboardMarkup);
		}
		sendMessage.setText(getBugMessage());
		return sendMessage;
	}

	/**
	 * фильтрация станций из списка по имени
	 * 
	 * @param stations
	 *            список станций
	 * @param stationName
	 *            название станции которое нужно найти
	 */
	private void filtrByStationNamePart(List<StationDto> stations, String stationName) {
		List<StationDto> stationsFiltr = new ArrayList<StationDto>();
		for (StationDto station : stations) {
			if (station.getName().toUpperCase().contains(stationName.toUpperCase())) {
				stationsFiltr.add(station);
			}
		}
		if (!stationsFiltr.isEmpty()) {
			stations.retainAll(stationsFiltr);
		}
	}

	/**
	 * Проверить текст на глобальные команды
	 * 
	 * @param message
	 *            сообщение
	 * @param state
	 *            состояние
	 * @return признак равен истене если найдена любая команда
	 * @throws TelegramApiException
	 */
	private boolean checkHomeCommand(Message message, State state) throws TelegramApiException {
		boolean res = false;
		SendMessage sendMessage;
		if (message.getText().startsWith("/")) {
			switch (message.getText()) {
			case Command.STOP_COMMAND:
				state.setDeleted(true);
				sendMessage = sendHideKeyboard(state, message.getMessageId());
				break;
			case Command.START_COMMAND:
				state.setDeleted(false);
				sendMessage = sendMessageDefault(message, state);
				break;
			case Command.SETTINGS_COMMAND:
				sendMessage = onSettingsChoosen(message, state);
				break;
			default:
				sendMessage = sendMessageDefault(message, state);
				break;
			}
			res = true;
			sendMessage(sendMessage);
		}
		if (message.hasText() && message.getText().equals(getHomeCommand())) {
			state.setType(StateType.MAIN_MENU);
			databaseService.save(state);
		}
		return res;
	}

	/**
	 * Отправить сообщение
	 * 
	 * @param sendMessage
	 *            сообщение
	 * @throws TelegramApiException
	 *             ошибка прни отправке сообщения
	 */
	private void sendMessage(SendMessage sendMessage) throws TelegramApiException {
		if (config.isWebhook()) {
			handlerWebhook.sendMessage(sendMessage);
		} else {
			handlerLongPolling.sendMessage(sendMessage);
		}
	}

	@Override
	public void archiveTicketsSend(ArchiveTicketsDto archiveTicketsDto) throws VerificationException {
		Map<String, List<ArchiveTicketDto>> key2ArchiveTickets = new HashMap<String, List<ArchiveTicketDto>>();
		for (ArchiveTicketDto archiveTicketDto : archiveTicketsDto.getListArchiveTicketDto()) {
			StringBuilder key = new StringBuilder();
			key.append(archiveTicketDto.getStationStart());
			key.append(archiveTicketDto.getStationEnd());
			key.append(archiveTicketDto.getStartDate());

			List<ArchiveTicketDto> listTicket = key2ArchiveTickets.get(key.toString());
			if (listTicket == null) {
				listTicket = new ArrayList<ArchiveTicketDto>();
				key2ArchiveTickets.put(key.toString(), listTicket);
			}
			if(!CarType.NO_DATA.equals(archiveTicketDto.getType())) {
				listTicket.add(archiveTicketDto);
			} 

		}
		if (restartDate == null) {
			restartDate = DateUtils.addDate(new Date(), Calendar.DATE, 1);
		} else {
			if (restartDate.before(new Date())) {
				restartDate = DateUtils.addDate(new Date(), Calendar.DATE, 1);
				count = 0;
			}
		}
		if (count < 20) {
			for (String key : key2ArchiveTickets.keySet()) {
				TripStats tripStats = applicationContext.getBean(TripStats.class);
				List<ArchiveTicketDto> archiveTickets = key2ArchiveTickets.get(key);
				if (!archiveTickets.isEmpty()) {
					tripStats.setArchiveTicketsDto(archiveTickets);
					tripStats.setSleepTimeMsec(count * 3 * 60 * 1000);
					try {
						taskService.addTasks(tripStats);
					} catch (Throwable t) {
						LOG.warn("Unable send stats golos: " + t.getMessage());
					}
					count++;
				}
			}
		} else {
			LOG.warn("Skip send notification GOLOS count: " + count + ", restart date: " + restartDate);
		}
	}

}
