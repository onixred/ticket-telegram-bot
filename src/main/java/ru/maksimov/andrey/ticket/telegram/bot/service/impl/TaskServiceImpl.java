package ru.maksimov.andrey.ticket.telegram.bot.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import ru.maksimov.andrey.ticket.telegram.bot.service.TaskService;

/**
 * Сервис по работе с обработчиком задач
 * 
 * @author amaksimov
 */
@Service
public class TaskServiceImpl implements TaskService {

	@Bean
	public TaskExecutor taskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(1);
		executor.setMaxPoolSize(2);
		executor.setQueueCapacity(30);
		return executor;
	}

	@Autowired
	private TaskExecutor taskExecutor;

	@Override
	public void addTasks(Runnable task) {
		taskExecutor.execute(task);
	}
}
