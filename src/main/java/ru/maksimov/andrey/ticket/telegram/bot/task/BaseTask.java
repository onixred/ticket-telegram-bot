package ru.maksimov.andrey.ticket.telegram.bot.task;

import java.util.Date;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bitcoinj.core.DumpedPrivateKey;
import org.bitcoinj.core.ECKey;

import ru.maksimov.andrey.golos4j.api.method.BroadcastTransactionSynchronous;
import ru.maksimov.andrey.golos4j.api.method.GetConfig;
import ru.maksimov.andrey.golos4j.api.method.GetDynamicGlobalProperties;
import ru.maksimov.andrey.golos4j.dto.ConfigDto;
import ru.maksimov.andrey.golos4j.dto.DynamicGlobalPropertiesDto;
import ru.maksimov.andrey.golos4j.dto.api.GetBroadcastTransactionSynchronousDto;
import ru.maksimov.andrey.golos4j.dto.api.GetConfigDto;
import ru.maksimov.andrey.golos4j.dto.api.GetDynamicGlobalPropertiesDto;
import ru.maksimov.andrey.golos4j.dto.operation.BaseOperation;
import ru.maksimov.andrey.golos4j.dto.operation.CommentDto;
import ru.maksimov.andrey.golos4j.dto.transaction.BaseTransactionDto;
import ru.maksimov.andrey.golos4j.socket.ServiceWebSocket;
import ru.maksimov.andrey.golos4j.util.TransactionUtil;
import ru.maksimov.andrey.golos4j.util.Util;

/**
 * Базовая задача для GOLOSа
 * 
 * @author amaksimov
 */
public abstract class BaseTask implements Runnable {

	private static final Logger LOG = LogManager.getLogger(BaseTask.class);

	static final String IMAGE = "http://storage9.static.itmages.ru/i/17/0829/h_1503978989_4133363_f0771e306d.png ";
	static final String HOST = "wss://ws.golos.io";
	static final String PRIVATE_KEY = "5Kj4gNYKTrXfKaPZVWDRWPTgjDPUjESy81JSn4JUf4QVymrGT2E";
	static final String LOGIN = "rzdfreeseatbot";
	static final String TAG = "free-seat4rzd";

	public GetBroadcastTransactionSynchronousDto sendPost(CommentDto commentDto) throws Exception {
		int id = 2;
		GetDynamicGlobalPropertiesDto getDynamicGlobalPropertiesDto = getDynamicGlobalProperties();
		DynamicGlobalPropertiesDto dynamicGlobalPropertiesDto = getDynamicGlobalPropertiesDto.getResults();

		BaseTransactionDto baseTransactionDto = new BaseTransactionDto();
		long headBlockNumber = dynamicGlobalPropertiesDto.getHeadBlockNumber();
		String headBlockId = dynamicGlobalPropertiesDto.getHeadBlockId();
		int refBlockNum = TransactionUtil.long2Last2Byte(headBlockNumber);
		baseTransactionDto.setRefBlockNum(refBlockNum);
		long refBlockPrefix = TransactionUtil.hexString2Long(headBlockId, 4);
		baseTransactionDto.setRefBlockPrefix(refBlockPrefix);
		Date time = dynamicGlobalPropertiesDto.getTime();
		Date expiration = Util.addTime(time, BaseTransactionDto.DEFAULT_EXPIRATION_TIME * 3);
		baseTransactionDto.setExpiration(expiration);
		List<BaseOperation> operations = baseTransactionDto.getOperations();
		operations.add(commentDto);

		GetConfigDto getConfigDto = getConfig();
		ConfigDto configDto = getConfigDto.getResults();
		String chainId = configDto.getSteemitChainId();
		ECKey postingKey = DumpedPrivateKey.fromBase58(null, PRIVATE_KEY).getKey();
		baseTransactionDto.setSignatures(chainId, postingKey);

		BroadcastTransactionSynchronous broadcastTransactionSynchronous = new BroadcastTransactionSynchronous(id,
				baseTransactionDto);
		GetBroadcastTransactionSynchronousDto gb = ServiceWebSocket.executePost(broadcastTransactionSynchronous,
				GetBroadcastTransactionSynchronousDto.class, HOST);
		return gb;
	}

	protected static GetDynamicGlobalPropertiesDto getDynamicGlobalProperties() throws Exception {
		LOG.info("Start getDynamicGlobalProperties");
		int id = 2;
		GetDynamicGlobalProperties getDynamicGlobalProperties = new GetDynamicGlobalProperties(id);
		GetDynamicGlobalPropertiesDto getDynamicGlobalPropertiesDto = ServiceWebSocket
				.executePost(getDynamicGlobalProperties, GetDynamicGlobalPropertiesDto.class, HOST);
		LOG.info("Stop getDynamicGlobalProperties");
		return getDynamicGlobalPropertiesDto;
	}

	protected static GetConfigDto getConfig() throws Exception {
		LOG.info("Start getConfig");
		int id = 2;
		GetConfig getConfig = new GetConfig(id);
		GetConfigDto getConfigDto = ServiceWebSocket
				.executePost(getConfig, GetConfigDto.class, HOST);
		LOG.info("Stop getConfig");
		return getConfigDto;
	}
}
