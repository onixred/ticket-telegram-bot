package ru.maksimov.andrey.ticket.telegram.bot.task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import ru.maksimov.andrey.golos4j.dto.api.GetBroadcastTransactionSynchronousDto;
import ru.maksimov.andrey.golos4j.dto.operation.CommentDto;
import ru.maksimov.andrey.golos4j.util.Util;

/**
 * Задача на отправку уведомления в GOLOS
 * 
 * @author amaksimov
 */
public class NotificationGolos extends BaseTask {

	private static final Logger LOG = LogManager.getLogger(NotificationGolos.class);

	private String head;
	private String message;

	@Override
	public void run() {
		try {
			LOG.info("Start NotificationGolos");
			if (StringUtils.isNotBlank(message)) {
				CommentDto commentDto = new CommentDto();
				String title = "РЖД бот обнаружил изменения:";
				if (StringUtils.isNotBlank(head)) {
					title += head;
				}
				String permlink = Util.title2Permlink(title);
				String body = "**Доброе время суток.** Я заметил изменение на сайте RZD. \n " + IMAGE + " \n "
						+ message + " \n Если ты хочешь чтобы я отследил нужный тебе рейс. \n "
						+ " Просто напиши комментарий: ОТКУДА -> КУДА, Дата отправления, тип вагона, тип места."
						+ "\n Например: АБАКАН -> МОСКВА отправление: 2017-09-18 вагона плацкартный, верхние боковые.";
				String tag = Util.text2Tag(head);
				Map<String, List<String>> key2value = new HashMap<String, List<String>>();
				List<String> tags = new ArrayList<String>();
				tags.add(tag);
				tags.add("rzd");
				List<String> images = new ArrayList<String>();
				images.add(IMAGE);

				key2value.put(CommentDto.TAGS_KEY, tags);
				key2value.put(CommentDto.IMAGE_KEY, images);
				// key2value.put(CommentDto.LINKS_KEY, value)
				commentDto.setAuthor(LOGIN);
				commentDto.setBody(body);
				commentDto.setJsonMetadata(key2value);
				commentDto.setParentAuthor("");
				commentDto.setParentPermlink(TAG);
				commentDto.setPermlink(permlink);
				commentDto.setTitle(title);

				GetBroadcastTransactionSynchronousDto gb = sendPost(commentDto);
				LOG.info("result broadcast transaction synchronous: " + gb);
			}
			LOG.info("Stop NotificationGolos");
		} catch (Exception e) {
			LOG.warn("Unable send message:" + e.getMessage(), e);
		}
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setHead(String head) {
		this.head = head;
	}
}
