package ru.maksimov.andrey.ticket.telegram.bot.task;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ru.maksimov.andrey.golos4j.dto.ErrorDto;
import ru.maksimov.andrey.golos4j.dto.api.GetBroadcastTransactionSynchronousDto;
import ru.maksimov.andrey.golos4j.dto.operation.CommentDto;
import ru.maksimov.andrey.golos4j.util.Util;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.ArchiveFreePlaceDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.ArchiveTicketDto;
import ru.maksimov.andrey.ticket.telegram.bot.service.ImageService;
import ru.maksimov.andrey.ticket.telegram.bot.utils.Text;

/**
 * Задача на отправку статистики в GOLOS
 * 
 * @author amaksimov
 */
@Component
@Scope("prototype")
public class TripStats extends BaseTask {

	private static final Logger LOG = LogManager.getLogger(TripStats.class);

	private List<ArchiveTicketDto> archiveTicketsDto;

	private long sleepTimeMsec = 0;

	@Autowired
	@Qualifier("XchartImageService")
	private ImageService imageService;

/*	@Autowired
	@Qualifier("GoogleImageService")
	private ImageService imageService2;
*/
	@Autowired
	@Qualifier("CloudinaryImageService")
	private ImageService imageService3;

	@Override
	public void run() {
		try {
			// между постами должен быть интекрвал
			Thread.sleep(sleepTimeMsec);
		} catch (Throwable t) {
			LOG.warn("Unable send stats golos: " + t.getMessage());
		}
		try {
			LOG.info("Start TripStats");
			if (archiveTicketsDto != null && !archiveTicketsDto.isEmpty()) {
				Map<String, Map<String, Map<String, Map<String, List<ArchiveFreePlaceDto>>>>> trip2Places = new HashMap<String, Map<String, Map<String, Map<String, List<ArchiveFreePlaceDto>>>>>();
				Map<String, List<ArchiveTicketDto>> trip2Tickets = new HashMap<String, List<ArchiveTicketDto>>();
				String tag = Text.DEFAULT_TAG;
				String head = Text.DEFAULT_FOLLOWING;
				for (ArchiveTicketDto archiveTicketDto : archiveTicketsDto) {
					StringBuilder trip = new StringBuilder();
					trip.append(archiveTicketDto.getStationStart());
					trip.append(Text.FOLLOWING_SIGN);
					trip.append(archiveTicketDto.getStationEnd());
					trip.append(Text.SPACE_SIGN);
					trip.append(Text.TRAIN);
					trip.append(Text.SPACE_SIGN);
					trip.append(archiveTicketDto.getTrainName());
					tag = archiveTicketDto.getStationStart() + Text.TO_SIGN + archiveTicketDto.getStationEnd();
					head = archiveTicketDto.getStationStart() + Text.FOLLOWING_SIGN + archiveTicketDto.getStationEnd() + Text.SPACE_SIGN
							+ archiveTicketDto.getStartDate();
					Map<String, Map<String, Map<String, List<ArchiveFreePlaceDto>>>> carType2places = trip2Places
							.get(trip.toString());
					if (carType2places == null) {
						carType2places = new HashMap<String, Map<String, Map<String, List<ArchiveFreePlaceDto>>>>();
						trip2Places.put(trip.toString(), carType2places);
					}

					Map<String, Map<String, List<ArchiveFreePlaceDto>>> placeType2places = carType2places
							.get(archiveTicketDto.getType().getName());
					if (placeType2places == null) {
						placeType2places = new HashMap<String, Map<String, List<ArchiveFreePlaceDto>>>();
						carType2places.put(archiveTicketDto.getType().getName(), placeType2places);
					}
					for (ArchiveFreePlaceDto freePlace : archiveTicketDto.getArchiveFreePlaces()) {
						Map<String, List<ArchiveFreePlaceDto>> carNumber2places = placeType2places
								.get(freePlace.getType().getMessage());
						if (carNumber2places == null) {
							carNumber2places = new HashMap<String, List<ArchiveFreePlaceDto>>();
							placeType2places.put(freePlace.getType().getMessage(), carNumber2places);
						}
						List<ArchiveFreePlaceDto> place = carNumber2places.get(archiveTicketDto.getCarName());
						if (place == null) {
							place = new ArrayList<ArchiveFreePlaceDto>();
							carNumber2places.put(archiveTicketDto.getCarName(), place);
						}
						place.add(freePlace);
					}

					List<ArchiveTicketDto> tickets = trip2Tickets.get(trip.toString());
					if (tickets == null) {
						tickets = new ArrayList<ArchiveTicketDto>();
						tickets.add(archiveTicketDto);
					}
					trip2Tickets.put(trip.toString(), tickets);
				}

				StringBuilder body = new StringBuilder();
				body.append(Text.HI_STAT_MESSAGE);
				body.append(Text.NL_SIGN);
				body.append(IMAGE);
				body.append(Text.NL_SIGN);

				for (String trip : trip2Places.keySet()) {
					Map<String, Map<String, Map<String, List<ArchiveFreePlaceDto>>>> carType2places = trip2Places
							.get(trip);
					for (String carType : carType2places.keySet()) {
						Map<String, Map<String, List<ArchiveFreePlaceDto>>> placeType2places = carType2places
								.get(carType);
						StringBuilder title = new StringBuilder();

						title.append(Text.TRIP);
						title.append(trip);
						title.append(Text.SPACE_SIGN);
						title.append(Text.CAR_TYPE);
						title.append(carType.toLowerCase());
						List<String> urls = imageService.getImageUrls(placeType2places, trip);

						body.append(title);
						body.append(Text.NL_SIGN);
						body.append(Text.PRICE_DYNAMICS);
						body.append(Text.NL_SIGN);
						if (urls.isEmpty() || urls.size() < 2) {
							urls = imageService3.getImageUrls(placeType2places, trip);
						}
						if (!urls.isEmpty()) {
							URL u1 = new URL(urls.get(0));
							body.append(u1);
						} else {
							body.append(Text.UNABLE_LOAD_IMAGE_MESSAGE);
						}

						body.append(Text.NL_SIGN);
						body.append(Text.EMPTY_SEATS_DYNAMICS);
						body.append(Text.NL_SIGN);
						if (urls.size() == 2) {
							URL u2 = new URL(urls.get(1));
							body.append(u2);
						} else {
							body.append(Text.UNABLE_LOAD_IMAGE_MESSAGE);
						}
						body.append(Text.NL_SIGN);
					}
				}

				body.append(Text.NL_SIGN);
				body.append(Text.ADD_TASK_1_MESSAGE);
				body.append(Text.NL_SIGN);
				body.append(Text.ADD_TASK_2_MESSAGE);
				body.append(Text.NL_SIGN);
				body.append(Text.EXAMPLE_ADD_TASK_MESSAGE);
				LOG.info(body.toString());
				if (!trip2Places.keySet().isEmpty()) {
					send(body.toString(), head, tag);
				}
			} else {
				LOG.warn("Skip Trip stats is empty");
			}
			LOG.info("Stop TripStats");
		} catch (Exception e) {
			LOG.warn("Unable send stats message:" + e.getMessage(), e);
		}
	}

	private void send(String message, String head, String trip) {
		try {
			LOG.info("Start NotificationGolos");
			if (StringUtils.isNotBlank(message)) {
				CommentDto commentDto = new CommentDto();
				String title = Text.DEFAULT_HEAD + Text.SPACE_SIGN;
				if (StringUtils.isNotBlank(head)) {
					title += head;
				}
				String permlink = Util.title2Permlink(title);
				String tag = Util.text2Tag(trip);
				Map<String, List<String>> key2value = new HashMap<String, List<String>>();
				List<String> tags = new ArrayList<String>();
				tags.add(tag);
				tags.add(Text.RZD_TAG);
				tags.add(Text.STATISTIKA_TAG);
				List<String> images = new ArrayList<String>();
				images.add(IMAGE);

				key2value.put(CommentDto.TAGS_KEY, tags);
				key2value.put(CommentDto.IMAGE_KEY, images);
				// key2value.put(CommentDto.LINKS_KEY, value)
				commentDto.setAuthor(LOGIN);
				commentDto.setBody(message);
				commentDto.setJsonMetadata(key2value);
				commentDto.setParentAuthor("");
				commentDto.setParentPermlink(TAG);
				commentDto.setPermlink(permlink);
				commentDto.setTitle(title);
				boolean isSeand = false;
				while(!isSeand) {
					try{
						GetBroadcastTransactionSynchronousDto gb = sendPost(commentDto);
						LOG.info("result broadcast transaction synchronous: " + gb);
						ErrorDto error = gb.getError();
						if(error == null) {
							isSeand = true;
						} 
					} catch (Exception e) {
						LOG.warn("result broadcast transaction synchronous: " + e.getMessage(), e);
					}
				}
			}
			LOG.info("Stop NotificationGolos");
		} catch (Exception e) {
			LOG.warn("Unable send message:" + e.getMessage(), e);
		}
	}

	public void setArchiveTicketsDto(List<ArchiveTicketDto> archiveTicketsDto) {
		this.archiveTicketsDto = archiveTicketsDto;
	}

	public void setSleepTimeMsec(long sleepTimeMsec) {
		this.sleepTimeMsec = sleepTimeMsec;
	}

}
