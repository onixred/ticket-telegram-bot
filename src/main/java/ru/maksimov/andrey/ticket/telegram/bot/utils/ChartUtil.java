package ru.maksimov.andrey.ticket.telegram.bot.utils;

/**
 * Вспомогаткельный класс для c диаграммами
 * 
 * @author amaksimov
 */
public class ChartUtil {

	/**
	 * Конвертация числа в символ для диаграммы
	 * 
	 * @param number
	 *            число (высота столбца)
	 * @return символ
	 */
	public static String int2char(int number) {
		char ch;
		if (number < 26) {
			// A-Z ASCII 65-90
			ch = (char) (number + 65);
		} else if (number < 52) {
			// A-Z ASCII 97-122
			ch = (char) (number + 71);
		} else if (number < 62) {
			// 0-9 ASCII 48-57
			ch = (char) (number - 4);
		} else {
			// 9 ASCII 57
			ch = 57;
		}
		return String.valueOf(ch);
	}

	/**
	 * Перевод из одной системы в другую
	 * 
	 * @param x
	 *            число небходимое преревести в другую систему
	 * @param min1
	 *            минимальное число шкалы 1
	 * @param max1
	 *            максимальное число шкалы 1
	 * @param min2
	 *            минимальное число шкалы 2
	 * @param max2
	 *            максимальное число шкалы 2
	 * @return число в системе 2
	 */
	public static int x2y(int x, int min1, int max1, int min2, int max2) {
		//избежать деление на ноль
		if (max1 - min1 == 0) {
			max1++;
		}
		int res = ((x - min1) * (max2 - min2)) / (max1 - min1) + min2;
		return res;
	}

	public static int x2y(double tariff, double minTariff, double maxTariff, int min2, int max2) {
		return x2y((int)tariff, (int)minTariff, (int)maxTariff, min2, max2);
	}
}
