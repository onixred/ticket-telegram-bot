package ru.maksimov.andrey.ticket.telegram.bot.utils;

/**
 * Общии команды для бота
 * 
 * @author amaksimov
 */
public class Command {

	public static final String COMMAND_INIT_CHAR = "/";
	public static final String STOP_COMMAND = COMMAND_INIT_CHAR + "stop";
	public static final String HELP_COMMAND = COMMAND_INIT_CHAR + "help";
	public static final String START_COMMAND = COMMAND_INIT_CHAR + "start";
	public static final String SETTINGS_COMMAND = COMMAND_INIT_CHAR + "settings";
	
}
