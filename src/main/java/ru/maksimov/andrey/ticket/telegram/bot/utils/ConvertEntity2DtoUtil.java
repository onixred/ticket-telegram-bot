package ru.maksimov.andrey.ticket.telegram.bot.utils;

import java.util.Collections;

import ru.maksimov.andrey.notification.ticket.purchase.client.dto.ContactDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.TaskDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.TimetableDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.UserDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.type.ContactType;
import ru.maksimov.andrey.ticket.telegram.bot.entity.TaskBot;

/**
 * Вспомогаткельный класс для конвертации сущностей и dto
 * 
 * @author amaksimov
 */
public class ConvertEntity2DtoUtil {

	/**
	 * Проверка задачи для бота
	 * 
	 * @param taskBot
	 * @return true если проверка прошла успешна иначе false
	 */
	public static boolean validation(TaskBot taskBot) {
		boolean isResult = true;
		isResult = taskBot.getStationStartId()!= null? true:false;
		isResult = taskBot.getStationEndId()!= null? true:false;
		isResult = taskBot.getStartDate()!= null? true:false;
		isResult = taskBot.getCarType()!= null? true:false;
		isResult = taskBot.getPlaceType()!= null? true:false;
		isResult = taskBot.getCountPlace()!= null? true:false;
		return isResult;
	}

	/**
	 * Проверка временной интервал оповещения
	 * 
	 * @param timetableDto
	 * @return true если проверка прошла успешна иначе false
	 */
	public static boolean validation(TimetableDto timetableDto) {
		boolean isResult = true;
		isResult = timetableDto.getStartTime()!= null? true:false;
		isResult = timetableDto.getEndTime()!= null? true:false;
		return isResult;
	}

	/**
	 * Конвертация из сущности задача для бота в DTO задачу
	 * 
	 * @param taskBot
	 *            задача для бота
	 * @param login
	 *            логин
	 * @return DTO задача
	 */
	public static TaskDto convertTaskDto2Сriterion(TaskBot taskBot, String login) {
		TaskDto taskDto = new TaskDto();
		taskDto.setStationStartId(taskBot.getStationStartId());
		taskDto.setStationEndId(taskBot.getStationEndId());
		taskDto.setStartDate(taskBot.getStartDate());
		taskDto.setCarType(taskBot.getCarType().getName());
		taskDto.setPlaceType(taskBot.getPlaceType().getName());
		taskDto.setCountPlace(taskBot.getCountPlace());
		taskDto.setLogin(login);
		return taskDto;
	}

	/**
	 * Создать dto пользователь с контактами телеграмм
	 * 
	 * @param login
	 *            логин (id состояния see ru.maksimov.andrey.ticket.telegram.bot.entity.State)
	 * @return пользователь
	 */
	public static UserDto createUserDto(Long login) {
		UserDto userDto = new UserDto();
		userDto.setLogin(login.toString());
		ContactDto contactDto = new ContactDto();
		contactDto.setType(ContactType.Telegram);
		contactDto.setContact(login.toString());
		userDto.setContacts(Collections.singleton(contactDto));
		return userDto;
	}
}
