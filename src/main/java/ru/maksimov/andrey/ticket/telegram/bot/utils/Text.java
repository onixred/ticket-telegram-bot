package ru.maksimov.andrey.ticket.telegram.bot.utils;

/**
 * Вспомогательный класс сообщений
 * 
 * @author amaksimov
 */
public class Text {

	public static final String CHOOSE = "Пожалуйста, выберите ";
	public static final String CHOOSE_OPTION = CHOOSE +" пункт ";
	public static final String ENTER_TEXT = "Пожалуйста, введите ";
	public static final String EXAMPLE_CITY = ", например 'Москва'.";
	public static final String EXAMPLE_DATE = ", в формате dd.MM.YYYY например '18.04.2017'.";
	public static final String EXAMPLE_TIMETABLE = ", в формате HH.mm HH.mm (часовой пояс МСК) например '8:10 15:40'.";
	public static final String ON_ADD_TASK_ERROR_RES = "Повторите ввод комманды. Произошла ошибка, %s";
	public static final String ON_SETTINGS_COMMAND = CHOOSE + "пункт \n\t\t%s Назад"
			+ "\n\t%s Настройки в разработке";


	public static final String ON_LIST_TASK = "Список ваших задач:";
	public static final String ON_SELECT_TASK = CHOOSE + "пункт \n\t\t%s Отменить задачу" + "\n\t\t%s Главное меню";

	public static final String ON_LIST_TIMETABLE = "Список времяных интервалов оповещений:";
	public static final String ON_DELETE_TIMETABLE = "Резултат удаления интервала оповещений: %s ";
	public static final String ON_ADD_TIMETABLE = ENTER_TEXT + "интервал оповещений:" + EXAMPLE_TIMETABLE;
	public static final String ON_ADD_TIMETABLE_END = "%s Ваш интервал оповещений добавлен";
	public static final String ON_ADD_TIMETABLE_LIMIT = "%s Ксожелению, вы достигли придела по интервал оповещений (%s штуки). Отмените интервал оповещений или свяжитесь с разработчиком";
	
	public static final String ON_ADD_TASKS_LIMIT = "%s Ксожелению, сервис достиг придела по задачам. Отмените задачу или свяжитесь с разработчиком. (Сервис находится в тесте)";
	public static final String ON_ADD_TASKS_LIMIT_FOR_USER = "%s Ксожелению, вы достигли придела по задачам (%s штуки). Отмените задачу или свяжитесь с разработчиком";
	public static final String ON_ADD_TASK_STATION_1 = ENTER_TEXT + "пункт отправления" + EXAMPLE_CITY;
	public static final String ON_ADD_TASK_STATION_2 = ENTER_TEXT + "пункт прбытия" + EXAMPLE_CITY;
	public static final String ON_ADD_TASK_DATE_START = ENTER_TEXT + "дату отправления" + EXAMPLE_DATE;
	public static final String ON_ADD_TASK_CAR_TYPE = CHOOSE + " тип вагона ";
	public static final String ON_ADD_TASK_PLACE_TYPE = CHOOSE + " тип места ";
	public static final String ON_ADD_TASK_COUNT_PLACE = ENTER_TEXT + " требуемое количество свободных мест ";
	public static final String ON_ADD_TASK_END = "%s Ваша задача добавлена";
	public static final String ON_ADD_TASK_END_ERROR = "Произошла ошибка. Данные по задаче не валидны.";
	public static final String ON_ADD_TASK_END_FAIL  = "Произошла ошибка при сохрранении. Нажмите повторить попытку";
	
	public static final String ON_ADD_TASK_STATION_1_LIST = CHOOSE +"станцию отправления, или введите заново:";
	public static final String ON_ADD_TASK_STATION_2_LIST = CHOOSE + "станцию прбытия, или введите заново:";
	
	public static final String ON_CANCEL_TASK = "Резултат отмены задачи: %s ";
	public static final String ON_INFO_TASK = "Резултат получения информации о местах: %s ";
	public static final String SUCCESS_MESSAGE ="выполнен успешно";

	public static final String HELP_WEATHER_MESSAGE = "Интересуешься билетами РЖД?\n "
			+ "Просто отправь мне оду из этих команд:" + "\n\t\t%sПолучить список твоих задач:"
			+ "\n\t\t%sДобавить задачу" + "\n\t\t%sИзменить настройки" + "\n\t\t%sОценить меня"
			+ "\nЭто не официальное приложение РЖД, всю информацию вы можете найти на официальном сайте rzd.ru.";
	public static final String RATE_ME_MESSAGE = "Если тебе нравится бот, ты можешь оценить его [@storebot](https://telegram.me/storebot?start=%s)";
	public static final String BUG_INFO_MESSAGE = "%s Если нашли баг, сообщите нам о нем [@AndreyMaksimov](https://telegram.me/AndreyMaksimov)";

	public static final String FILTR_STATION_RUS_MESSAGE = "[^0-9а-яА-Я, \\-\\]\\[]";
	public static final String FILTR_DATE_RUS_MESSAGE = "[^0-9\\.,\\- ]";
	public static final String FILTR_TIME_RUS_MESSAGE = "[^0-9: ]";
	public static final String FILTR_DIGIT_MESSAGE = "[^0-9]";

	public static final String WARN_LIMIT_MESSAGE = "%s Внимание всего %s из них будет показано %s";


	public static final String DEFAULT_TAG = "abakan2moskva";
	public static final String RZD_TAG = "rzd";
	public static final String STATISTIKA_TAG = "ru--statistika";
	
	
	public static final String DEFAULT_FOLLOWING  = "Казань => Москва 25.11.2017";
	public static final String DEFAULT_HEAD  = "я подготовил для вас статистику:";

	
	public static final String SPACE_SIGN  = " ";
	public static final String FOLLOWING_SIGN  = "=>";
	public static final String TO_SIGN  = "2";
	public static final String NL_SIGN  = "\n";

	public static final String TRAIN  = "поезд:";
	public static final String TRIP  = "Рейс:";
	public static final String CAR_TYPE  = "тип вагона:";
	public static final String PRICE_DYNAMICS  = "Динамика цен:";
	public static final String EMPTY_SEATS_DYNAMICS =" Динамика свободных мест:";
	
	public static final String HI_STAT_MESSAGE = "**Доброе время суток.** Я провел анализ рейcа.";
	
	public static final String UNABLE_LOAD_IMAGE_MESSAGE ="** Невозможно загрузить изображение! **";

	public static final String ADD_TASK_1_MESSAGE = "Ты хочешь чтобы я отследил и сделал анализ нужного тебе рейса?";
	public static final String ADD_TASK_2_MESSAGE = "Просто напиши комментарий к этому посту: ОТКУДА -> КУДА, Дата отправления, тип вагона, тип места.";

	public static final String EXAMPLE_ADD_TASK_MESSAGE ="Например: АБАКАН -> МОСКВА отправление: 2017-09-18 вагона плацкартный, верхние боковые.";
}
