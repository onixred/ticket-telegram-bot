package ru.maksimov.andrey.ticket.telegram.bot;

/**
 * Тестовый класс для проверки Regexа
 * 
 * @author amaksimov
 */
public class RegexTest {

	public static void main(String[] args) {
		String s = "   [выаыа] ываыа, ыаыа dsfsdf sfsf3453453 345345 22!#$%m....".replaceAll("[^0-9а-яА-Я, \\]\\[]", "");
		System.out.println(s);
	}

}
