package ru.maksimov.andrey.ticket.telegram.bot;


/**
 * Тестовый стартер проекта
 * 
 * @author amaksimov
 */
public class TestStarter {

	public static void main(final String args[]) throws Throwable {
		Starter.main(args);
	}
}
