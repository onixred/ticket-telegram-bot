package ru.maksimov.andrey.ticket.telegram.bot.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.maksimov.andrey.commons.exception.BusinessException;
import ru.maksimov.andrey.commons.utils.DateUtils;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.ArchiveFreePlaceDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.ArchiveTicketDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.type.CarType;
import ru.maksimov.andrey.notification.ticket.purchase.client.type.PlaceType;
import ru.maksimov.andrey.ticket.telegram.bot.service.impl.GoogleImageService;

public class GoogleImageServiceTest {
	public static void main(String[] args) throws BusinessException {

		List<ArchiveTicketDto> archiveTicketsDto = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			ArchiveTicketDto archiveTicketDto = new ArchiveTicketDto();
			archiveTicketsDto.add(archiveTicketDto);
			archiveTicketDto.setStartDate(new Date());
			String stationStart;
			String stationEnd;
			CarType type;
			if (i % 2 == 0) {
				stationStart = "Казань";
				stationEnd = "Абакан";
				type = CarType.CORRIDOR_CAACH;
			} else {
				stationStart = "Москва";
				stationEnd = "Красноярск";
				type = CarType.COUCHETTE;
			}
			archiveTicketDto.setStationStart(stationStart);
			archiveTicketDto.setStationEnd(stationEnd);
			archiveTicketDto.setType(type);
			List<ArchiveFreePlaceDto> archiveFreePlaces = new ArrayList<ArchiveFreePlaceDto>();
			archiveTicketDto.setArchiveFreePlaces(archiveFreePlaces);
			archiveTicketDto.setCarName(Integer.toString(i));
			for (int j = 0; j < 100; j++) {
				ArchiveFreePlaceDto archiveFreePlaceDto = new ArchiveFreePlaceDto();
				archiveFreePlaces.add(archiveFreePlaceDto);
				Date date = DateUtils.addDate(new Date(), Calendar.DATE, j);
				archiveFreePlaceDto.setDateUpdate(date);
				int freeNumber = (int) (Math.random() * (80 - 2) + 1);
				archiveFreePlaceDto.setFreeNumber(freeNumber);
				double tariff = (int) (Math.random() * (1800 - 1750) + 1);
				archiveFreePlaceDto.setTariff(tariff);
				double tariffAlternative = (int) (Math.random() * (1800 - 1750));
				archiveFreePlaceDto.setTariffAlternative(tariffAlternative);
				PlaceType typePlace;
				if (j % 2 == 0) {
					typePlace = PlaceType.DOWEN;
				} else {
					typePlace = PlaceType.KUPE;
				}
				archiveFreePlaceDto.setType(typePlace);
			}
		}

		Map<String, Map<String, Map<String, Map<String, List<ArchiveFreePlaceDto>>>>> trip2Places = new HashMap<String, Map<String, Map<String, Map<String, List<ArchiveFreePlaceDto>>>>>();
		for (ArchiveTicketDto archiveTicketDto : archiveTicketsDto) {
			String trip = archiveTicketDto.getStationStart() + "=>" + archiveTicketDto.getStationEnd();
			Map<String, Map<String, Map<String, List<ArchiveFreePlaceDto>>>> carType2places = trip2Places.get(trip);
			if (carType2places == null) {
				carType2places = new HashMap<String, Map<String, Map<String, List<ArchiveFreePlaceDto>>>>();
				trip2Places.put(trip, carType2places);
			}

			Map<String, Map<String, List<ArchiveFreePlaceDto>>> placeType2places = carType2places
					.get(archiveTicketDto.getType().getName());
			if (placeType2places == null) {
				placeType2places = new HashMap<String, Map<String, List<ArchiveFreePlaceDto>>>();
				carType2places.put(archiveTicketDto.getType().getName(), placeType2places);
			}

			for (ArchiveFreePlaceDto freePlace : archiveTicketDto.getArchiveFreePlaces()) {

				Map<String, List<ArchiveFreePlaceDto>> carName2places = placeType2places
						.get(freePlace.getType().getName());
				if (carName2places == null) {
					carName2places = new HashMap<String, List<ArchiveFreePlaceDto>>();
					placeType2places.put(freePlace.getType().getName(), carName2places);
				}

				List<ArchiveFreePlaceDto> place = carName2places.get(archiveTicketDto.getCarName());
				if (place == null) {
					place = new ArrayList<ArchiveFreePlaceDto>();
					carName2places.put(archiveTicketDto.getCarName(), place);
				}
				place.add(freePlace);
			}
		}
		GoogleImageService imageService = new GoogleImageService();
		for (String trip : trip2Places.keySet()) {
			Map<String, Map<String, Map<String, List<ArchiveFreePlaceDto>>>> carType2places = trip2Places.get(trip);
			for (String carType : carType2places.keySet()) {
				List<String> urls = imageService.getImageUrls(carType2places.get(carType), trip);
				for (String url : urls) {
					System.out.println(url);
				}
			}
		}

	}
}
