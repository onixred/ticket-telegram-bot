package ru.maksimov.andrey.ticket.telegram.bot.task;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import ru.maksimov.andrey.commons.utils.DateUtils;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.ArchiveFreePlaceDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.dto.ArchiveTicketDto;
import ru.maksimov.andrey.notification.ticket.purchase.client.type.CarType;
import ru.maksimov.andrey.notification.ticket.purchase.client.type.PlaceType;

/**
 * Тестовый Задача на отправку статистики в GOLOS
 * 
 * @author amaksimov
 */
public class TripStatsTest {

	public static void main(String[] args) {
		List<ArchiveTicketDto> archiveTicketsDto = new ArrayList<>();
		for (int i = 0; i < 5; i++) {
			ArchiveTicketDto archiveTicketDto = new ArchiveTicketDto();
			archiveTicketsDto.add(archiveTicketDto);
			archiveTicketDto.setStartDate(new Date());
			String stationStart;
			String stationEnd;
			CarType type;
			if (i % 2 == 0) {
				stationStart = "Казань";
				stationEnd = "Абакан";
				type = CarType.CORRIDOR_CAACH;
			} else {
				stationStart = "Москва";
				stationEnd = "Красноярск";
				type = CarType.COUCHETTE;
			}
			archiveTicketDto.setStationStart(stationStart);
			archiveTicketDto.setStationEnd(stationEnd);
			archiveTicketDto.setType(type);
			List<ArchiveFreePlaceDto> archiveFreePlaces = new ArrayList<ArchiveFreePlaceDto>();
			archiveTicketDto.setArchiveFreePlaces(archiveFreePlaces);
			for (int j = 0; j < 100; j++) {
				ArchiveFreePlaceDto archiveFreePlaceDto = new ArchiveFreePlaceDto();
				archiveFreePlaces.add(archiveFreePlaceDto);
				Date date = DateUtils.addDate(new Date(), Calendar.DATE, j);
				archiveFreePlaceDto.setDateUpdate(date);
				int freeNumber = (int) (Math.random() * (80- 1));
				archiveFreePlaceDto.setFreeNumber(freeNumber);
				double tariff = (int) (Math.random() * (1800 - 1200));
				archiveFreePlaceDto.setTariff(tariff);
				double tariffAlternative = (int) (Math.random() * (2500 - 2200));
				archiveFreePlaceDto.setTariffAlternative(tariffAlternative);
				PlaceType typePlace;
				if (j % 2 == 0) {
					typePlace = PlaceType.DOWEN;
				} else {
					typePlace = PlaceType.KUPE;
				}
				archiveFreePlaceDto.setType(typePlace);
			}
		}

		TripStats tripStats = new TripStats();
		tripStats.setArchiveTicketsDto(archiveTicketsDto);
		tripStats.run();
	}
}
