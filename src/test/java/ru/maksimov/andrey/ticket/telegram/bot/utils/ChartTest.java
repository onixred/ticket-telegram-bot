package ru.maksimov.andrey.ticket.telegram.bot.utils;

import java.awt.Color;

import ru.maksimov.andrey.ticket.telegram.bot.utils.ChartUtil;

/**
 * Тестовый класс для проверки вспомогательного класса диаграмм
 * 
 * @author amaksimov
 */
public class ChartTest {

	public static void main(String[] args) {
		for (int i = 0; i < 70; i++) {
			String str = ChartUtil.int2char(i);
			System.out.println(i + " st: " + str);
		}
		// http://chartpart.com/
		System.out.println(Color.blue.toString());
	}
}
